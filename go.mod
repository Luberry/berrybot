module gitlab.com/luberry/berrybot

go 1.16

require (
	github.com/TwinProduction/go-away v1.4.0
	github.com/bwmarrin/discordgo v0.27.2-0.20230922130345-1f0b57f11024
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1
	github.com/karrick/tparse/v2 v2.8.2
	github.com/kr/pretty v0.1.0 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	mvdan.cc/xurls/v2 v2.3.0 // indirect
)
