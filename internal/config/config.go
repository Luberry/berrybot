package config

import (
	"context"
	"errors"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"

	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/join"
	"gitlab.com/luberry/berrybot/internal/util"

	"github.com/bwmarrin/discordgo"
	"github.com/hashicorp/go-multierror"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
)

const defaultDir = "./cfg"
const Enabled = "Enabled"

var Directory = func() string {
	c := os.Getenv("CONFIG_DIR")
	if c == "" {
		return defaultDir
	}
	return c
}()

var CleanCommands = func() bool {
	c := os.Getenv("CLEAN_CMDS")
	val, _ := strconv.ParseBool(c)
	return val
}()

var ignoreModStatus = func() bool {
	c := os.Getenv("IGNORE_MODERATORS")
	val, _ := strconv.ParseBool(c)
	return val
}()

func init() {
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetReportCaller(true)
	logrus.SetLevel(logrus.InfoLevel)
}

type Configureable interface {
	Defaults()
	Init(ctx context.Context) error
	SetAppInfo(inf *app.Info)
	Name() string
}

var configs = make(map[string]int)

var configList []Configureable

func MemberIsMod(m *discordgo.Member) bool {
	if ignoreModStatus {
		return false
	}
	bt := &Bot{}
	idx, ok := configs[bt.Name()]
	if !ok {
		logrus.Error("bot not configured")
		return false
	}
	b, ok := configList[idx].(*Bot)
	if !ok {
		logrus.Error("incorrect config for bot")
		return false
	}
	return b.MemberIsMod(m)
}

func Register(c Configureable) {
	name := c.Name()
	if name == "" {
		logrus.Fatal("invalid configuration name: must be a non empty string")
	}
	log := logrus.WithField("name", name)
	if c == nil {
		log.Fatal("configuration must not be nil")
	}
	log = logrus.WithField("config", c)
	if idx, ok := configs[name]; ok {
		log.Warn("Overwriting config registered at this name")
		configList[idx] = c
	} else {
		configs[name] = len(configList)
		configList = append(configList, c)
	}
	log.Info("registered configuration")
}

func Load(ctx context.Context) {
	ForEach(func(c Configureable) error {
		log := logrus.WithField("name", c.Name())
		LoadConfig(c)

		if err := c.Init(ctx); err != nil {
			log.WithError(err).Error("failed initializing config")
		}
		log.Info("loaded config")
		return nil
	})
}
func LoadConfig(c Configureable) {
	log := logrus.WithField("name", c.Name())
	log.Info("loading config")
	raw, err := ioutil.ReadFile(configFile(c))
	if errors.Is(err, fs.ErrNotExist) {
		c.Defaults()
		if err := SaveConfig(c); err != nil {
			log.WithError(err).Fatal("cannot save default config")
		}
	}
	if err := yaml.Unmarshal(raw, c); err != nil {
		log.WithError(err).Fatal("could not load configuration")
	}
}

func configFile(c Configureable) string {
	return filepath.Join(Directory, c.Name()+".yml")
}

func SaveConfig(c Configureable) error {
	data, err := yaml.Marshal(c)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(configFile(c), data, 0700)
}

func ForEach(f func(c Configureable) error) error {
	errs := &multierror.Error{}
	for _, c := range configList {
		c := c
		errs = multierror.Append(errs, f(c))
	}
	return errs.ErrorOrNil()
}

var _ Configureable = &Bot{}

// TODO: maybe have feature toggles
type Bot struct {
	log         *logrus.Entry
	Log         Log            `yaml:"log"`
	BotName     string         `yaml:"name"`
	AppInfo     app.Info       `yaml:",inline"`
	Token       string         `yaml:"token"`
	Debug       Debug          `yaml:"debug,omitempty"`
	Moderation  app.Moderation `yaml:"moderation"`
	IntroConfig join.Config    `yaml:"introConfig"`
}

type Log struct {
	Level string `yaml:"level"`
	JSON  bool   `yaml:"json"`
}

func (l Log) Defaults() {
	l.Level = logrus.InfoLevel.String()
	l.JSON = true
}

type Debug struct {
}

func (d Debug) Defaults() {
}

func (b *Bot) LogLevel() logrus.Level {
	lvl, err := logrus.ParseLevel(b.Log.Level)
	if err != nil {
		logrus.WithError(err).Error("could not set log level. defaulting to info level")
		lvl = logrus.InfoLevel
	}
	return lvl
}

func (b *Bot) MemberIsMod(m *discordgo.Member) bool {
	for _, role := range b.Moderation.Roles {
		if util.MemberHasRole(m, &discordgo.Role{ID: role}) {
			return true
		}
	}
	return false
}

func (b *Bot) LogJSON() bool {
	return b.Log.JSON
}

func (b *Bot) Defaults() {
	b.Log.Defaults()
	b.Debug.Defaults()
	b.Moderation.Defaults()
	b.BotName = "BerryBot"
	b.Token = ""
	b.AppInfo.Defaults()
	b.IntroConfig.Defaults()
}
func (b *Bot) Init(context.Context) error {
	b.log = logrus.WithField("handler", b.Name())
	lvl := b.LogLevel()
	logrus.SetLevel(lvl)
	b.log.Logger.SetLevel(lvl)
	if b.LogJSON() {
		logrus.SetFormatter(&logrus.JSONFormatter{})
		b.log.Logger.SetFormatter(&logrus.JSONFormatter{})
	} else {
		logrus.SetFormatter(&logrus.TextFormatter{})
		b.log.Logger.SetFormatter(&logrus.TextFormatter{})
	}
	return nil
}
func (b *Bot) Name() string {
	return "bot"
}
func (b *Bot) SetAppInfo(info *app.Info) {
	// noop since bot has this config
}
