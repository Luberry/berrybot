package app

type Info struct {
	AppID         string `yaml:"appID"`
	GuildID       string `yaml:"guildID"`
	GuildCommands bool   `yaml:"guildCommands"`
	ServerName    string `yaml:"serverName"`
}

func (i *Info) Defaults() {
	i.AppID = ""
	i.GuildID = ""
	i.GuildCommands = false
}
