package app

type Moderation struct {
	GuildCommands bool     `yaml:"guildCommands"`
	Roles         []string `yaml:"roles"`
}

func (m *Moderation) Defaults() {
	m.GuildCommands = true
	m.Roles = []string{}
}
