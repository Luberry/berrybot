package util

import "github.com/bwmarrin/discordgo"

func MemberHasRole(m *discordgo.Member, r *discordgo.Role) bool {
	if m == nil || r == nil {
		return false
	}
	for _, role := range m.Roles {
		if r.ID == role {
			return true
		}
	}
	return false
}
