package util

import (
	"errors"

	"github.com/bwmarrin/discordgo"
	"github.com/hashicorp/go-multierror"
	"github.com/sirupsen/logrus"
)

func InteractionRespond(s *discordgo.Session, i *discordgo.Interaction, content string) error {
	if err := s.InteractionRespond(i, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: content,
		},
	}); err != nil {
		logrus.WithError(err).Warn("interaction timed out")
		if _, err := s.ChannelMessageSend(i.ChannelID, content); err != nil {
			return err
		}
	}
	return nil
}

type RespondOpt func(opts *respondOpts)
type respondOpts struct {
	content         string
	fallbackContent string
	fallbackChannel string
}

func WithContent(msg string) RespondOpt {
	return func(opts *respondOpts) {
		opts.content = msg
	}
}

func WithFallbackContent(msg string) RespondOpt {
	return func(opts *respondOpts) {
		opts.fallbackContent = msg
	}
}
func WithFallbackChannel(c string) RespondOpt {
	return func(opts *respondOpts) {
		opts.fallbackChannel = c
	}
}
func DmRespond(s *discordgo.Session, u *discordgo.User, opts ...RespondOpt) error {
	ro := &respondOpts{}
	for _, opt := range opts {
		opt(ro)
	}
	if len(ro.content) == 0 {
		return errors.New("invalid message")
	}
	if len(ro.fallbackChannel) == 0 && len(ro.fallbackContent) > 0 {
		return errors.New("invalid fallback channel")
	}
	errs := &multierror.Error{}
	msg := ro.content
	c, err := s.UserChannelCreate(u.ID)
	if err != nil {
		errs = multierror.Append(errs, err)
		if len(ro.fallbackContent) > 0 {
			msg = ro.fallbackContent
		}
		c = &discordgo.Channel{ID: ro.fallbackChannel}
	}

	if _, err := s.ChannelMessageSend(c.ID, msg); err != nil {
		errs = multierror.Append(errs, err)
	}
	return errs.ErrorOrNil()
}

func StringPTR(s string) *string {
	return &s
}
