package util

import "github.com/bwmarrin/discordgo"

func GetInteractionOptString(m map[string]*discordgo.ApplicationCommandInteractionDataOption, key string) string {
	opt, ok := m[key]
	if !ok || opt.Type != discordgo.ApplicationCommandOptionString {
		return ""
	}
	return opt.StringValue()
}

func GetInteractionOptBool(m map[string]*discordgo.ApplicationCommandInteractionDataOption, key string) bool {
	opt, ok := m[key]
	if !ok || opt.Type != discordgo.ApplicationCommandOptionBoolean {
		return false
	}
	return opt.BoolValue()
}
func GetInteractionOptInt(m map[string]*discordgo.ApplicationCommandInteractionDataOption, key string) int {
	opt, ok := m[key]
	if !ok || opt.Type != discordgo.ApplicationCommandOptionInteger {
		return 0
	}
	return int(opt.IntValue())
}

func GetInteractionOptChannel(s *discordgo.Session, m map[string]*discordgo.ApplicationCommandInteractionDataOption, key string) *discordgo.Channel {
	opt, ok := m[key]
	if !ok || opt.Type != discordgo.ApplicationCommandOptionChannel {
		return nil
	}
	return opt.ChannelValue(s)
}
