package keywordredirect

import (
	"context"
	"strings"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"

	"gitlab.com/luberry/berrybot/internal/commands"
	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/messages"
)

var handler config.Configureable = &Handler{}
var _ messages.Handler = handler.(*Handler)
var _ commands.Handler = handler.(*Handler)

func init() {
	config.Register(handler)
}

type Trigger struct {
	TargetChannels []string `yaml:"targetChannels"`
	Message        string   `yaml:"message"`
	Words          []string `yaml:"words"`
}

type Handler struct {
	Enabled          bool      `yaml:"enabled"`
	Triggers         []Trigger `yaml:"triggers"`
	triggerMap       map[string]map[string]string
	appInfo          *app.Info
	log              *logrus.Entry
	DebounceDuration time.Duration `yaml:"debounceDuration"`
	lastTrigger      map[string]map[string]time.Time
	m                sync.Mutex
}

func (h *Handler) Defaults() {
	h.triggerMap = make(map[string]map[string]string)
	h.Enabled = false
}

func (h *Handler) SetAppInfo(inf *app.Info) {
	h.appInfo = inf
}

func (h *Handler) Name() string {
	return "keywordRedirect"
}

func (h *Handler) Init(_ context.Context) error {
	h.m.Lock()
	defer h.m.Unlock()
	h.log = logrus.WithField("name", h.Name())
	h.triggerMap = make(map[string]map[string]string)
	h.lastTrigger = make(map[string]map[string]time.Time)
	for _, trig := range h.Triggers {
		for _, word := range trig.Words {
			word = strings.ToLower(word)
			for _, ch := range trig.TargetChannels {
				if _, ok := h.triggerMap[ch]; !ok {
					h.triggerMap[ch] = make(map[string]string)
				}
				h.triggerMap[ch][word] = trig.Message
			}
		}
	}
	return nil
}

func (h *Handler) MessageAdd() func(s *discordgo.Session, m *discordgo.MessageCreate) {
	if !h.Enabled {
		return nil
	}
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		log := h.log.WithField("message", m)
		if m.Author.Bot || config.MemberIsMod(m.Member) {
			return
		}
		content := strings.ToLower(m.Content)
		words, ok := h.triggerMap[m.ChannelID]
		if !ok || len(words) == 0 {
			return
		}
		lastTriggerWords := h.lastTrigger[m.ChannelID]
		for word := range words {
			now := time.Now()
			lastTrigger := lastTriggerWords[word]
			if h.DebounceDuration > 0 && lastTriggerWords != nil && !now.After(lastTrigger.Add(h.DebounceDuration)) {
				continue
			}
			if !strings.Contains(content, word) {
				continue
			}
			h.m.Lock()
			if _, ok := h.lastTrigger[m.ChannelID]; !ok {
				h.lastTrigger[m.ChannelID] = make(map[string]time.Time)
			}
			h.lastTrigger[m.ChannelID][word] = now
			h.m.Unlock()
			if _, err := s.ChannelMessageSendReply(m.ChannelID, words[word], m.Reference()); err != nil {
				log.WithError(err).WithField("replyMessage", words[word]).Error("failed to reply to message")
			}

		}

	}
}
func (h *Handler) MessageRemove() func(s *discordgo.Session, m *discordgo.MessageDelete) {
	return nil
}

func (h *Handler) Commands(s *discordgo.Session) []*discordgo.ApplicationCommand {
	return nil
}
func (h *Handler) Handler(name string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	return nil
}
func (h *Handler) ModCommands(s *discordgo.Session) []*discordgo.ApplicationCommand {
	return nil
}

func (h *Handler) ModHandler(name string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	return nil
}
func (h *Handler) LegacyHandler(cmd string) func(s *discordgo.Session, m *discordgo.MessageCreate) {
	return nil
}
