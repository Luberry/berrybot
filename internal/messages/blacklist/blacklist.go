package blacklist

import (
	"bytes"
	"context"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"

	goaway "github.com/TwinProduction/go-away"
	"gitlab.com/luberry/berrybot/internal/commands"
	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/messages"
	"gitlab.com/luberry/berrybot/internal/util"
)

var handler config.Configureable = &Handler{}
var _ messages.Handler = handler.(*Handler)
var _ commands.Handler = handler.(*Handler)

func init() {
	config.Register(handler)
}

type userViolation struct {
	Name       string         `yaml:"name"`
	Warnings   map[string]int `yaml:"warnings"`
	Violations map[string]int `yaml:"violations"`
}

type Handler struct {
	Enabled                            bool     `yaml:"enabled"`
	Channel                            string   `yaml:"channel"`
	IgnoredChannels                    []string `yaml:"ignoredChannels"`
	Blacklist                          []string `yaml:"blacklist"`
	blacklist                          map[string]bool
	Whitelist                          []string `yaml:"whitelist"`
	whitelist                          map[string]bool
	violations                         violations
	appInfo                            *app.Info
	log                                *logrus.Entry
	violationDetector, warningDetector *goaway.ProfanityDetector
	ignoredChannels                    map[string]bool
	m                                  sync.Mutex
}

type violations map[string]*userViolation

func (v *violations) Defaults() {
}
func (v *violations) SetAppInfo(_ *app.Info) {}

func (v *violations) Name() string {
	return handler.Name() + "Violations"
}
func (v *violations) Init(_ context.Context) error { return nil }

func (h *Handler) Defaults() {
	h.violations = make(violations)
	h.Blacklist = []string{}
	h.Whitelist = []string{}
	h.IgnoredChannels = []string{}
	h.Enabled = true
}

func (h *Handler) SetAppInfo(inf *app.Info) {
	h.appInfo = inf
}

func (h *Handler) Name() string {
	return "blacklist"
}

var defaultWhitelist []string = append(goaway.DefaultFalsePositives,
	"scrape",
	"spice",
)

func (h *Handler) initDetectors() {
	h.warningDetector = goaway.NewProfanityDetector().WithCustomDictionary(h.Blacklist, append(h.Whitelist, defaultWhitelist...), []string{})
	h.violationDetector = goaway.NewProfanityDetector().WithCustomDictionary(h.Blacklist, append(h.Whitelist, defaultWhitelist...), []string{}).
		WithSanitizeSpaces(false).
		WithSanitizeLeetSpeak(false).
		WithSanitizeSpecialCharacters(false).
		WithSanitizeAccents(false)

}
func (h *Handler) Init(_ context.Context) error {
	h.log = logrus.WithField("name", h.Name())
	h.whitelist = make(map[string]bool)
	h.initDetectors()
	h.blacklist = make(map[string]bool)
	h.ignoredChannels = make(map[string]bool)
	for _, c := range h.IgnoredChannels {
		h.ignoredChannels[c] = true
	}
	for _, c := range h.Blacklist {
		h.blacklist[c] = true
	}
	for _, c := range h.Whitelist {
		h.whitelist[c] = true
	}
	config.LoadConfig(&h.violations)
	return nil
}

func (h *Handler) alertMods(s *discordgo.Session, m *discordgo.Message, word string, warn bool) error {
	msg := "----------------------\n"
	if !warn {
		msg = fmt.Sprintf(
			"%s Removed message with violation (`%s`) in <#%s> posted by @%s :\n ```%s```",
			msg, word, m.ChannelID, m.Author.Username, m.Content,
		)
	} else {
		msg = fmt.Sprintf(
			"%s Potential violation (`%s`) in <#%s> posted by @%s :\n ```%s```",
			msg, word, m.ChannelID, m.Author.Username, m.Content,
		)
	}
	_, err := s.ChannelMessageSend(h.Channel, msg)
	return err
}

func extractProfanity(d *goaway.ProfanityDetector, content string) (string, bool) {
	w := d.ExtractProfanity(content)
	return w, len(w) > 0
}

var space = regexp.MustCompile(`\s+`)

func (h *Handler) MessageAdd() func(s *discordgo.Session, m *discordgo.MessageCreate) {
	if !h.Enabled {
		return nil
	}
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		log := h.log.WithField("message", m)
		if m.Author.Bot || config.MemberIsMod(m.Member) || h.ignoredChannels[m.ChannelID] {
			return
		}
		var (
			w              string
			violated, warn bool
		)
		content := strings.ToLower(space.ReplaceAllString(m.Content, " "))
		violateFunc := func(w string) {
			if err := s.ChannelMessageDelete(m.ChannelID, m.ID); err != nil {
				log.WithError(err).Error("could not delete blacklisted message")
			}
			msg := fmt.Sprintf(
				"Please refrain from using language like `%s` on the `%s` server.\n```%s```",
				w, h.appInfo.ServerName, m.Content)
			alt := fmt.Sprintf("%s, please refrain from using that language on this server.", m.Author.Mention())
			if err := util.DmRespond(s, m.Author,
				util.WithFallbackContent(alt),
				util.WithContent(msg),
				util.WithFallbackChannel(m.ChannelID)); err != nil {
				log.WithError(err).Error("failed to send blacklist warning message")
			}
			if err := h.alertMods(s, m.Message, w, false); err != nil {
				log.WithError(err).Error("failed to alert mods")
			}
		}
		if w, violated = extractProfanity(h.violationDetector, content); violated {
			violateFunc(w)
		} else if w, warn = extractProfanity(h.warningDetector, content); warn {
			var b strings.Builder
			b.WriteString(`(^|\s)`)
			for idx, r := range w {
				b.WriteRune(r)
				if idx+1 < len(w) {
					b.WriteString(`\s?`)
				}
			}
			b.WriteString(`(\s|$)`)
			rexp, err := regexp.Compile(b.String())
			if err != nil {
				log.WithError(err).Error("failed to compile rexp")
			}
			if rexp.MatchString(content) {
				violated = true
				violateFunc(w)
			} else if err := h.alertMods(s, m.Message, w, true); err != nil {
				log.WithError(err).Error("failed to alert mods")
			}
		}
		aid := m.Author.ID
		h.m.Lock()
		defer h.m.Unlock()
		if _, ok := h.violations[aid]; !ok {
			h.violations[aid] = &userViolation{}
		}
		h.violations[aid].Name = m.Author.Username
		if violated {
			if h.violations[aid].Violations == nil {
				h.violations[aid].Violations = make(map[string]int)
			}
			h.violations[aid].Violations[w]++
		} else if warn {
			if h.violations[aid].Warnings == nil {
				h.violations[aid].Warnings = make(map[string]int)
			}
			h.violations[aid].Warnings[w]++
		}
		if err := config.SaveConfig(&h.violations); err != nil {
			log.WithError(err).Error("could not save violations")
		}
	}
}
func (h *Handler) MessageRemove() func(s *discordgo.Session, m *discordgo.MessageDelete) {
	return nil
}

const (
	blacklist        = "blacklist"
	add              = "add"
	remove           = "remove"
	show             = "show"
	ignored          = "ignored"
	logCmd           = "log"
	userViolations   = "violations"
	userWarnings     = "warnings"
	disabledChannels = "disabled-channels"
	ignore           = "ignore"
	unignore         = "unignore"
	toggle           = "toggle"
	chn              = "channel"
	enable           = "enable"
	word             = "word"
	usr              = "usr"
)

var (
	modCommands = []*discordgo.ApplicationCommand{
		{
			Name:        blacklist,
			Description: "manage blacklisted terms and violations",

			Options: []*discordgo.ApplicationCommandOption{
				{
					Name:        toggle,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "toggle blacklist handling in this channel",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        chn,
							Description: "channel to toggle blacklist watching",
							Type:        discordgo.ApplicationCommandOptionChannel,
							Required:    true,
						},
						{
							Name:        ignore,
							Description: "toggles whether the channel is to be ignored",
							Type:        discordgo.ApplicationCommandOptionBoolean,
							Required:    false,
						},
					},
				},
				{
					Name:        add,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "add a word to the blacklist",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        word,
							Description: "word to add to blacklist",
							Type:        discordgo.ApplicationCommandOptionString,
							Required:    true,
						},
					},
				},
				{
					Name:        remove,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "remove a word from the blacklist",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        word,
							Description: "word to remove from blacklist",
							Type:        discordgo.ApplicationCommandOptionString,
							Required:    true,
						},
					},
				},
				{
					Name:        ignore,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "add a word to ignore",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        word,
							Description: "word to add to whitelist",
							Type:        discordgo.ApplicationCommandOptionString,
							Required:    true,
						},
					},
				},
				{
					Name:        unignore,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "remove a word from the whitelist",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        word,
							Description: "word to remove from whitelist",
							Type:        discordgo.ApplicationCommandOptionString,
							Required:    true,
						},
					},
				},
				{
					Name:        userViolations,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "list a user's violations",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        usr,
							Description: "offending user",
							Type:        discordgo.ApplicationCommandOptionUser,
							Required:    true,
						},
					},
				},
				{
					Name:        userWarnings,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "list a user's warnings",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        usr,
							Description: "offending user",
							Type:        discordgo.ApplicationCommandOptionUser,
							Required:    true,
						},
					},
				},
				{
					Name:        logCmd,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "log of all violations",
				},
				{
					Name:        show,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "list all blacklisted words",
				},
				{
					Name:        ignored,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "list all ignored words",
				},
				{
					Name:        disabledChannels,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "list disabled channels",
				},
			},
		},
	}
)

func (h *Handler) Commands(s *discordgo.Session) []*discordgo.ApplicationCommand {
	return nil
}
func (h *Handler) Handler(name string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	return nil
}
func (h *Handler) ModCommands(s *discordgo.Session) []*discordgo.ApplicationCommand {
	if !h.Enabled {
		return nil
	}
	return modCommands
}

func removeString(key string, data []string) []string {
	for idx := len(data) - 1; idx >= 0; idx-- {
		if data[idx] == key {
			return append(data[:idx], data[idx+1:]...)
		}
	}
	return data
}

func (h *Handler) ModHandler(name string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	if !h.Enabled {
		return nil
	}
	switch name {
	case blacklist:
		return func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			h := h
			content := ""
			var f []*discordgo.File
			cmd := i.ApplicationCommandData().Options[0]
			opts := cmd.Options
			switch cmd.Name {
			case toggle:
				h.m.Lock()
				defer h.m.Unlock()
				util.InteractionRespond(s, i.Interaction, "toggling blacklist handling for specified channel")
				if len(opts) < 1 || opts[0].Name != chn || opts[0].Type != discordgo.ApplicationCommandOptionChannel {
					content = "must specify at least the name of the channel to toggle"
					break
				}
				chn := opts[0].ChannelValue(s)
				if chn == nil {
					content = "must specify at least the name of the channel to toggle"
					break
				}
				options := make(map[string]*discordgo.ApplicationCommandInteractionDataOption)
				for _, opt := range opts[1:] {
					options[opt.Name] = opt
				}
				ign, found := h.ignoredChannels[chn.ID]
				ign = !ign
				if e, ok := options[ignore]; ok && options[ignore].Type == discordgo.ApplicationCommandOptionBoolean {
					ign = e.BoolValue()
				}

				if !ign {
					delete(h.ignoredChannels, chn.ID)
					if found {
						h.IgnoredChannels = removeString(chn.ID, h.IgnoredChannels)
					}
					content = fmt.Sprintf("blacklist enabled in channel %s", chn.Mention())
					break
				}
				h.ignoredChannels[chn.ID] = true
				if !found {
					h.IgnoredChannels = append(h.IgnoredChannels, chn.ID)
				}
				if err := config.SaveConfig(h); err != nil {
					content = err.Error()
					break
				}
				content = fmt.Sprintf("blacklist disabled in channel %s", chn.Mention())
			case add:
				h.m.Lock()
				defer h.m.Unlock()
				util.InteractionRespond(s, i.Interaction, "adding word to blacklist")
				if len(opts) != 1 || opts[0].Name != word || opts[0].Type != discordgo.ApplicationCommandOptionString {
					content = "invalid options for blacklist add"
					break
				}
				wrd := strings.ToLower(opts[0].StringValue())
				if !h.blacklist[wrd] {
					h.blacklist[wrd] = true
					h.Blacklist = append(h.Blacklist, wrd)
					if err := config.SaveConfig(h); err != nil {
						content = err.Error()
						break
					}
					h.initDetectors()
				}
				content = fmt.Sprintf("blacklisted word `%s`", wrd)
			case remove:
				h.m.Lock()
				defer h.m.Unlock()
				util.InteractionRespond(s, i.Interaction, "removing word from blacklist")
				if len(opts) != 1 || opts[0].Name != word || opts[0].Type != discordgo.ApplicationCommandOptionString {
					content = "invalid options for blacklist remove"
					break
				}
				wrd := strings.ToLower(opts[0].StringValue())
				if h.blacklist[wrd] {
					delete(h.blacklist, wrd)
					h.Blacklist = removeString(wrd, h.Blacklist)
					if err := config.SaveConfig(h); err != nil {
						content = err.Error()
						break
					}
					h.initDetectors()
				}
				content = fmt.Sprintf("removed word `%s` from blacklist", wrd)
			case ignore:
				h.m.Lock()
				defer h.m.Unlock()
				util.InteractionRespond(s, i.Interaction, "ignoreing word to blacklist")
				if len(opts) != 1 || opts[0].Name != word || opts[0].Type != discordgo.ApplicationCommandOptionString {
					content = "invalid options for blacklist ignore"
					break
				}
				wrd := strings.ToLower(opts[0].StringValue())
				if !h.whitelist[wrd] {
					h.whitelist[wrd] = true
					h.Whitelist = append(h.Whitelist, wrd)
					if err := config.SaveConfig(h); err != nil {
						content = err.Error()
						break
					}
					h.initDetectors()
				}
				content = fmt.Sprintf("whitelisted word `%s`", wrd)
			case unignore:
				h.m.Lock()
				defer h.m.Unlock()
				util.InteractionRespond(s, i.Interaction, "removing word from whitelist")
				if len(opts) != 1 || opts[0].Name != word || opts[0].Type != discordgo.ApplicationCommandOptionString {
					content = "invalid options for blacklist unignore"
					break
				}
				wrd := strings.ToLower(opts[0].StringValue())
				if h.whitelist[wrd] {
					delete(h.whitelist, wrd)
					h.Whitelist = removeString(wrd, h.Whitelist)
					if err := config.SaveConfig(h); err != nil {
						content = err.Error()
						break
					}
					h.initDetectors()
				}
				content = fmt.Sprintf("removed word `%s` from whitelist", wrd)
			case userWarnings:
				util.InteractionRespond(s, i.Interaction, "fetching list of user warnings")
				if len(opts) != 1 || opts[0].Name != usr || opts[0].Type != discordgo.ApplicationCommandOptionUser {
					content = "invalid options for user warnings"
					break
				}
				u := opts[0].UserValue(s)
				w, ok := h.violations[u.ID]
				if !ok || len(w.Warnings) == 0 {
					content = fmt.Sprintf("user @%s has no warnings", u.Username)
					break
				}
				var b strings.Builder
				b.WriteString("__**Here is a list of all words flagged for a warning posted by @")
				b.WriteString(u.Username)
				b.WriteString(":**__")
				for wrd, c := range w.Warnings {
					b.WriteString("\n`")
					b.WriteString(wrd)
					b.WriteString("`: ")
					b.WriteString(strconv.Itoa(c))
				}
				content = b.String()
			case userViolations:
				util.InteractionRespond(s, i.Interaction, "fetching list of user violations")
				if len(opts) != 1 || opts[0].Name != usr || opts[0].Type != discordgo.ApplicationCommandOptionUser {
					content = "invalid options for user violations"
					break
				}
				u := opts[0].UserValue(s)
				w, ok := h.violations[u.ID]
				if !ok || len(w.Warnings) == 0 {
					content = fmt.Sprintf("user @%s has no violations", u.Username)
					break
				}
				var b strings.Builder
				b.WriteString("__**Here is a list of blacklisted words posted by by @")
				b.WriteString(u.Username)
				b.WriteString(":**__")
				for wrd, c := range w.Violations {
					b.WriteString("\n`")
					b.WriteString(wrd)
					b.WriteString("`: ")
					b.WriteString(strconv.Itoa(c))
				}
				content = b.String()
			case ignored, show:
				tp := "ignored"
				words := h.Whitelist
				if cmd.Name == show {
					tp = "blacklisted"
					words = h.Blacklist
				}
				util.InteractionRespond(s, i.Interaction, fmt.Sprintf("fetching list of %s words", tp))
				var b strings.Builder
				b.WriteString("__**Here is a list of ")
				b.WriteString(tp)
				b.WriteString("words:**__")
				for _, wrd := range words {
					b.WriteString("\n`")
					b.WriteString(wrd)
					b.WriteRune('`')
				}
				content = b.String()
			case disabledChannels:
				util.InteractionRespond(s, i.Interaction, "fetching list of disabled channels")
				var b strings.Builder
				b.WriteString("__**Here is a list of disabled channels:**__")
				for _, chn := range h.IgnoredChannels {
					b.WriteString("\n`")
					b.WriteString(chn)
					b.WriteRune('`')
				}
				content = b.String()
			case logCmd:
				util.InteractionRespond(s, i.Interaction, "fetching violation log")
				data, err := yaml.Marshal(h.violations)
				if err != nil {
					content = err.Error()
					break
				}
				f = []*discordgo.File{{
					Name:   fmt.Sprintf("violations-%s.yml", time.Now().Format("2006-01-02")),
					Reader: bytes.NewReader(data),
				}}
				content = "here is the current violations log"
			default:
				content = fmt.Sprintf("invalid subcommand %s", cmd.Name)
			}
			if _, err := s.InteractionResponseEdit(i.Interaction, &discordgo.WebhookEdit{Content: util.StringPTR(content), Files: f}); err != nil {
				h.log.WithError(err).WithField("interaction", i).Error("failed to send response")
			}

		}
	}
	return nil
}
func (h *Handler) LegacyHandler(cmd string) func(s *discordgo.Session, m *discordgo.MessageCreate) {
	return nil
}
