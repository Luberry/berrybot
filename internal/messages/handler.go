package messages

import (
	"github.com/bwmarrin/discordgo"
)

type Handler interface {
	MessageAdd() func(s *discordgo.Session, m *discordgo.MessageCreate)
	MessageRemove() func(s *discordgo.Session, m *discordgo.MessageDelete)
}

func Register(s *discordgo.Session, h Handler) error {
	s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if h := h.MessageAdd(); h != nil {
			h(s, m)
			return
		}
	})
	s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageDelete) {
		if h := h.MessageRemove(); h != nil {
			h(s, m)
			return
		}
	})
	return nil
}
