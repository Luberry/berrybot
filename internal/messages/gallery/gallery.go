package gallery

import (
	"context"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"

	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/messages"
)

var handler config.Configureable = &Handler{}
var _ messages.Handler = handler.(*Handler)

func init() {
	config.Register(handler)
}

type Handler struct {
	Enabled bool   `yaml:"enabled"`
	Channel string `yaml:"channel"`
	appInfo *app.Info
	log     *logrus.Entry
}

func (h *Handler) Defaults() {
	h.Enabled = false
	h.Channel = ""
}

func (h *Handler) SetAppInfo(inf *app.Info) {
	h.appInfo = inf
}

func (h *Handler) Name() string {
	return "gallery"
}
func (h *Handler) Init(_ context.Context) error {
	h.log = logrus.WithField("name", h.Name())
	return nil
}

func (h *Handler) MessageAdd() func(s *discordgo.Session, m *discordgo.MessageCreate) {
	if !h.Enabled {
		return nil
	}
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if m.ChannelID != h.Channel {
			return
		}
		log := h.log.WithField("message", m)
		log.Info("message posted in gallery")
		if config.MemberIsMod(m.Member) {
			return
		}
		image := false
		for _, a := range m.Attachments {
			if strings.Contains(a.ContentType, "image/") {
				image = true
				break
			}
		}
		if image {
			return
		}
		if err := s.ChannelMessageDelete(m.ChannelID, m.ID); err != nil {
			log.WithError(err).Error("could not delete non image post in gallery")
			return
		}
		log.Info("deleted non image post in gallery")

	}
}
func (h *Handler) MessageRemove() func(s *discordgo.Session, m *discordgo.MessageDelete) {
	return nil
}
