package blacklist

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
	"mvdan.cc/xurls/v2"

	"github.com/patrickmn/go-cache"
	"gitlab.com/luberry/berrybot/internal/commands"
	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/messages"
	"gitlab.com/luberry/berrybot/internal/util"
)

var handler config.Configureable = &Handler{}
var _ messages.Handler = handler.(*Handler)
var _ commands.Handler = handler.(*Handler)

const (
	defaultExpiry = 5 * time.Minute
	defaultClean  = 10 * time.Minute
	defaultLimit  = 3
)

var urlDetect = xurls.Strict()

var defaultLinkFilters = []string{
	"nitro",
	"steam",
	"crypto",
}

func init() {
	config.Register(handler)
}

type userViolation struct {
	Name       string   `yaml:"name"`
	Violations int      `yaml:"violations"`
	Content    []string `yaml:"content"`
}

type Handler struct {
	Enabled             bool          `yaml:"enabled"`
	Channel             string        `yaml:"channel"`
	Expiry              time.Duration `yaml:"expiry"`
	Limit               int           `yaml:"limit"`
	Cleanup             time.Duration `yaml:"cleanup"`
	AdditionalSafeRoles []string      `yaml:"additionalSafeRoles"`
	violations          violations
	LinkFilters         LinkFilters `yaml:"linkFilters"`
	appInfo             *app.Info
	log                 *logrus.Entry
	violationCache      *cache.Cache
	m                   sync.Mutex
}
type LinkFilters struct {
	Enabled bool     `yaml:"enabled"`
	Words   []string `yaml:"words"`
}

type violations map[string]*userViolation

func (v *violations) Defaults() {
}
func (v *violations) SetAppInfo(_ *app.Info) {}

func (v *violations) Name() string {
	return handler.Name() + "Violations"
}
func (v *violations) Init(_ context.Context) error { return nil }

func (h *Handler) Defaults() {
	h.violations = make(violations)
	h.AdditionalSafeRoles = []string{}
	h.Enabled = true
	h.Expiry = defaultExpiry
	h.Cleanup = defaultClean
	h.Limit = defaultLimit
	h.LinkFilters.Enabled = true
}

func (h *Handler) SetAppInfo(inf *app.Info) {
	h.appInfo = inf
}

func (h *Handler) Name() string {
	return "scamfilter"
}

func (h *Handler) Init(_ context.Context) error {
	h.log = logrus.WithField("name", h.Name())
	h.violationCache = cache.New(h.Expiry, h.Cleanup)
	if h.LinkFilters.Enabled {
		h.LinkFilters.Words = append(h.LinkFilters.Words, defaultLinkFilters...)
		for idx := range h.LinkFilters.Words {
			h.LinkFilters.Words[idx] = strings.ToLower(h.LinkFilters.Words[idx])
		}
	}
	config.LoadConfig(&h.violations)
	return nil
}

func (h *Handler) alertMods(s *discordgo.Session, m *discordgo.Message) error {
	msg := "----------------------\n"
	msg = fmt.Sprintf(
		"%s Banned user @%s for tagging everyone or here in multiple channels too quickly, last message:\n ```%s```",
		msg, m.Author.Username, m.Content,
	)
	_, err := s.ChannelMessageSend(h.Channel, msg)
	return err
}

func (h *Handler) isSafeRole(m *discordgo.Member) bool {
	for _, r := range h.AdditionalSafeRoles {
		if util.MemberHasRole(m, &discordgo.Role{ID: r}) {
			return true
		}
	}
	return false
}

func (l LinkFilters) shouldFlag(m *discordgo.MessageCreate) bool {
	if !l.Enabled || !urlDetect.Match([]byte(m.Content)) {
		return false
	}
	c := strings.ToLower(m.Content)
	for _, w := range l.Words {
		if strings.Contains(c, w) {
			return true
		}
	}
	return false
}

func (h *Handler) MessageAdd() func(s *discordgo.Session, m *discordgo.MessageCreate) {
	if !h.Enabled {
		return nil
	}
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		log := h.log.WithField("message", m)
		if !h.LinkFilters.shouldFlag(m) && !strings.Contains(m.Content, "@everyone") && !strings.Contains(m.Content, "@here") {
			return
		}
		if m.Author.Bot || config.MemberIsMod(m.Member) || h.isSafeRole(m.Member) {
			return
		}

		aid := m.Author.ID
		v, ok := h.violationCache.Get(aid + "count")
		msg, _ := m.ContentWithMoreMentionsReplaced(s)
		if !ok {
			h.violationCache.SetDefault(aid, []string{msg})
			h.violationCache.SetDefault(aid+"count", 1)
			return
		} else if iv, _ := v.(int); iv+1 < h.Limit {
			content, _ := h.violationCache.Get(aid)
			clist, _ := content.([]string)
			h.violationCache.SetDefault(aid, append(clist, msg))
			h.violationCache.Increment(aid+"count", 1)
			return
		}
		content, _ := h.violationCache.Get(aid)
		clist, _ := content.([]string)

		iv, _ := v.(int)
		seen := make(map[string]bool)
		vio := userViolation{
			Name:       m.Author.Username,
			Violations: iv + 1,
		}
		for _, c := range clist {
			if seen[c] {
				continue
			}
			seen[c] = true
			vio.Content = append(vio.Content, c)
		}
		if err := s.GuildBanCreateWithReason(m.GuildID, m.Author.ID, "potential scam bot auto ban", 1); err != nil {
			log.WithField("author", m.Author.Username).WithError(err).Error("could not ban user")
		}
		if err := h.alertMods(s, m.Message); err != nil {
			log.WithError(err).Error("failed alerting mods")
		}
		h.m.Lock()
		defer h.m.Unlock()
		h.violations[aid] = &vio
		config.SaveConfig(&h.violations)
	}
}
func (h *Handler) MessageRemove() func(s *discordgo.Session, m *discordgo.MessageDelete) {
	return nil
}
func (h *Handler) Commands(s *discordgo.Session) []*discordgo.ApplicationCommand {
	return nil
}
func (h *Handler) Handler(name string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	return nil
}
func (h *Handler) ModCommands(s *discordgo.Session) []*discordgo.ApplicationCommand {
	if !h.Enabled {
		return nil
	}
	return modCommands
}

const (
	scamfilter = "scamfilter"
	logCmd     = "log"
)

var (
	modCommands = []*discordgo.ApplicationCommand{
		{
			Name:        scamfilter,
			Description: "scamfilter commands",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Name:        logCmd,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "log of all violations",
				},
			},
		},
	}
)

func (h *Handler) ModHandler(name string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	if !h.Enabled {
		return nil
	}
	switch name {
	case scamfilter:
		return func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			h := h
			content := ""
			var f []*discordgo.File
			cmd := i.ApplicationCommandData().Options[0]
			switch cmd.Name {
			case logCmd:
				util.InteractionRespond(s, i.Interaction, "fetching violation log")
				data, err := yaml.Marshal(h.violations)
				if err != nil {
					content = err.Error()
					break
				}
				f = []*discordgo.File{{
					Name:   fmt.Sprintf("violations-%s.yml", time.Now().Format("2006-01-02")),
					Reader: bytes.NewReader(data),
				}}
				content = "here is the current violations log"
			default:
				content = fmt.Sprintf("invalid subcommand %s", cmd.Name)
			}
			if _, err := s.InteractionResponseEdit(i.Interaction, &discordgo.WebhookEdit{Content: util.StringPTR(content), Files: f}); err != nil {
				h.log.WithError(err).WithField("interaction", i).Error("failed to send response")
			}

		}
	}
	return nil
}

func (h *Handler) LegacyHandler(cmd string) func(s *discordgo.Session, m *discordgo.MessageCreate) {
	return nil
}
