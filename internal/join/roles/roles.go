package roles

import (
	"context"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"

	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/join"
)

var handler config.Configureable = &Handler{}
var _ join.Handler = handler.(*Handler)

func init() {
	config.Register(handler)
}

type Handler struct {
	Enabled     bool          `yaml:"enabled"`
	MemberRoles []string      `yaml:"memberRoles"`
	JoinDelay   time.Duration `yaml:"joinDelay"`
	appInfo     *app.Info
	log         *logrus.Entry
}

func (h *Handler) Defaults() {
	h.Enabled = false
	h.MemberRoles = []string{}
	h.JoinDelay = 1 * time.Minute
}

func (h *Handler) SetAppInfo(inf *app.Info) {
	h.appInfo = inf
}

func (h *Handler) Name() string {
	return "joinRoles"
}
func (h *Handler) Init(_ context.Context) error {
	h.log = logrus.WithField("name", h.Name())
	return nil
}
func (h *Handler) MemberAdd() func(s *discordgo.Session, m *discordgo.GuildMemberAdd) {
	if !h.Enabled {
		return nil
	}
	return func(s *discordgo.Session, m *discordgo.GuildMemberAdd) {
		h := h
		h.log.WithField("user", m.User.Username).Info("New member joined")
		roles := h.MemberRoles
		msg := "You'll be granted a member role very soon to access the rest of the server."
		if err := join.IntroMessage(s, m.Member, msg); err != nil {
			h.log.WithError(err).WithField("member", m).Error("failed to send intro message")
		}
		time.Sleep(h.JoinDelay)
		for _, role := range roles {
			if err := s.GuildMemberRoleAdd(m.GuildID, m.User.ID, role); err != nil {
				h.log.WithField("role", role).WithField("user", m.User.ID).WithError(err).Error("failed adding role to member on join")
				continue
			}
		}
	}
}
func (h *Handler) MemberRemove() func(s *discordgo.Session, m *discordgo.GuildMemberRemove) {
	return nil
}
