package welcome

import (
	"context"
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"

	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/join"
)

var handler config.Configureable = &Handler{}
var _ join.Handler = handler.(*Handler)

func init() {
	config.Register(handler)
}

// TODO: use templating to make this more powerful maybe?
const mention = "{{mention}}"

type Handler struct {
	Enabled      bool     `yaml:"enabled"`
	Messages     []string `yaml:"messages"`
	RulesChannel string   `yaml:"rulesChannel"`
	appInfo      *app.Info
	log          *logrus.Entry
}

func (h *Handler) Defaults() {
	h.Enabled = true
	h.RulesChannel = ""
	h.Messages = []string{}
}

func (h *Handler) SetAppInfo(inf *app.Info) {
	h.appInfo = inf
}

func (h *Handler) Name() string {
	return "joinWelcome"
}
func (h *Handler) Init(_ context.Context) error {
	h.log = logrus.WithField("name", h.Name())
	return nil
}
func (h *Handler) MemberAdd() func(s *discordgo.Session, m *discordgo.GuildMemberAdd) {
	if !h.Enabled {
		return nil
	}
	return func(s *discordgo.Session, m *discordgo.GuildMemberAdd) {
		h := h
		h.log.WithField("user", m.User.Username).Info("New member joined")
		var msg string
		if len(h.Messages) == 0 {
			rules := "the rules."
			if h.RulesChannel != "" {
				rules = fmt.Sprintf("<#%s>", h.RulesChannel)
			}
			msg = fmt.Sprintf("Welcome, %s ! Be sure to read through %s.", m.Mention(), rules)
		} else {
			rand.Seed(time.Now().UnixNano())
			msg = strings.ReplaceAll(h.Messages[rand.Intn(len(h.Messages))], mention, m.Mention())
		}
		if err := join.IntroMessage(s, m.Member, msg); err != nil {
			h.log.WithError(err).WithField("member", m).Error("failed to send welcome message")
		}
	}
}
func (h *Handler) MemberRemove() func(s *discordgo.Session, m *discordgo.GuildMemberRemove) {
	return nil
}
