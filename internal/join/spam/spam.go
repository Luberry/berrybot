package spam

import (
	"context"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"

	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/join"
)

var handler config.Configureable = &Handler{}
var _ join.Handler = handler.(*Handler)

func init() {
	config.Register(handler)
}

type Handler struct {
	Enabled   bool     `yaml:"enabled"`
	Blacklist []string `yaml:"blacklist"`
	appInfo   *app.Info
	log       *logrus.Entry
}

func (h *Handler) Defaults() {
	h.Enabled = true
	h.Blacklist = []string{}
}

func (h *Handler) SetAppInfo(inf *app.Info) {
	h.appInfo = inf
}

func (h *Handler) Name() string {
	return "joinSpam"
}
func (h *Handler) Init(_ context.Context) error {
	h.log = logrus.WithField("name", h.Name())
	return nil
}
func (h *Handler) MemberAdd() func(s *discordgo.Session, m *discordgo.GuildMemberAdd) {
	if !h.Enabled {
		return nil
	}
	return func(s *discordgo.Session, m *discordgo.GuildMemberAdd) {
		h := h
		spam := false
		lowerName := strings.ToLower(m.User.Username)
		for _, str := range h.Blacklist {
			str = strings.ToLower(str)
			if strings.Contains(lowerName, str) {
				spam = true
				break
			}
		}
		if !spam {
			return
		}
		log := h.log.WithField("member", m)
		log.Info("removing spambot")
		msg := "Spambots are not welcome in this server. If you believe this was in error, remove the URL or spam phrase from your username before rejoining."
		if c, err := s.UserChannelCreate(m.User.ID); err != nil {
			log.WithError(err).Error("failed to send spam bot warning message")
		} else if _, err := s.ChannelMessageSend(c.ID, msg); err != nil {
			log.WithError(err).Error("failed to send spam bot warning message")
		}
		if err := s.GuildMemberDeleteWithReason(m.GuildID, m.User.ID, msg); err != nil {
			log.WithError(err).Error("failed to kick spam bot")
		}
	}
}
func (h *Handler) MemberRemove() func(s *discordgo.Session, m *discordgo.GuildMemberRemove) {
	return nil
}
