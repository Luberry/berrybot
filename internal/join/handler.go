package join

import (
	"github.com/bwmarrin/discordgo"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
)

type Handler interface {
	MemberAdd() func(s *discordgo.Session, m *discordgo.GuildMemberAdd)
	MemberRemove() func(s *discordgo.Session, m *discordgo.GuildMemberRemove)
}

type Config struct {
	Channel string `yaml:"channel"`
	DMS     bool   `yaml:"dms"`
}

func (i *Config) Defaults() {
	i.Channel = ""
	i.DMS = false
}

var cfg *Config

func Register(s *discordgo.Session, h Handler, ic *Config) error {
	s.Identify.Intents = discordgo.IntentsAll
	cfg = ic

	s.AddHandler(func(s *discordgo.Session, a *discordgo.GuildMemberAdd) {
		h := h
		if f := h.MemberAdd(); f != nil {
			f(s, a)
		}
	})
	s.AddHandler(func(s *discordgo.Session, r *discordgo.GuildMemberRemove) {
		h := h
		if f := h.MemberRemove(); f != nil {
			f(s, r)
		}
	})
	return nil
}

func IntroMessage(s *discordgo.Session, m *discordgo.Member, msg string) error {
	// we can't fall back on a dm error, so don't message
	if cfg == nil || cfg.Channel == "" {
		return nil
	}
	errs := &multierror.Error{}
	if cfg.DMS {
		if c, err := s.UserChannelCreate(m.User.ID); err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "could create dm with user, falling back to intro channel"))
		} else if _, err := s.ChannelMessageSend(c.ID, msg); err != nil {
			errs = multierror.Append(errs, errors.Wrap(err, "could not dm user, falling back to intro channel"))
		}
		if errs.ErrorOrNil() == nil {
			return nil
		}
	}
	_, err := s.ChannelMessageSend(cfg.Channel, msg)
	return multierror.Append(errs, err).ErrorOrNil()
}
