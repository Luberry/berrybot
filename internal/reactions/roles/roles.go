package roles

import (
	"context"
	"strconv"
	"sync"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/reactions"
	"gitlab.com/luberry/berrybot/internal/util"
)

var handler config.Configureable = &Handler{}
var _ reactions.Handler = handler.(*Handler)

func init() {
	config.Register(handler)
}

type set struct {
	TriggerID string `yaml:"triggerId"`
	// map[emoteId]role
	mappings map[string]string
	Mappings []*mapping `yaml:"mappings"`
}
type mapping struct {
	Role  string `yaml:"role"`
	Emote string `yaml:"emote"`
}

type Handler struct {
	appInfo *app.Info
	Enabled bool   `yaml:"enabled"`
	Sets    []*set `yaml:"sets"`
	// map of msg id to set id
	SetMappings     map[string]int `yaml:"setMappings"`
	triggerMappings map[string]int
	Channel         string `yaml:"channel"`
	m               sync.Mutex
	log             *logrus.Entry
}

func (h *Handler) SetAppInfo(inf *app.Info) {
	h.appInfo = inf
}

func (h *Handler) Defaults() {
	h.Enabled = true
	h.SetMappings = make(map[string]int)
	h.Sets = []*set{}
	h.Channel = ""
}
func (h *Handler) Init(ctx context.Context) error {
	h.log = logrus.WithField("name", h.Name())
	h.triggerMappings = make(map[string]int)
	if h.SetMappings == nil {
		h.SetMappings = make(map[string]int)
	}
	for idx, st := range h.Sets {
		h.triggerMappings[st.TriggerID] = idx
		h.Sets[idx].mappings = make(map[string]string)
		for _, m := range st.Mappings {
			h.Sets[idx].mappings[m.Emote] = m.Role
		}
	}
	return nil
}
func (h *Handler) Name() string {
	return "roleReactions"
}

func (h *Handler) setupMessage(s *discordgo.Session, a *discordgo.MessageReactionAdd, id int) error {
	h.m.Lock()
	defer h.m.Unlock()
	h.SetMappings[a.MessageID] = id
	if err := s.MessageReactionsRemoveAll(a.ChannelID, a.MessageID); err != nil {
		return err
	}
	for _, m := range h.Sets[id].Mappings {
		emid := m.Emote

		if _, err := strconv.Atoi(m.Emote); err == nil {
			em, err := s.GuildEmoji(a.GuildID, m.Emote)
			if err != nil {
				return errors.Wrapf(err, "emote (%q)", m.Emote)
			}
			emid = em.APIName()
		}
		if err := s.MessageReactionAdd(a.ChannelID, a.MessageID, emid); err != nil {
			return errors.Wrapf(err, "emote  could not be added (%q)", m.Emote)
		}
	}

	return config.SaveConfig(h)
}
func (h *Handler) handleMessageReact(s *discordgo.Session, a *discordgo.MessageReactionAdd, id int) error {
	emid := a.Emoji.ID
	if emid == "" {
		emid = a.Emoji.Name
	}
	st := h.Sets[id]
	rid, ok := st.mappings[emid]
	if !ok {
		return nil
	}
	m, err := s.GuildMember(a.GuildID, a.UserID)
	if err != nil {
		return err
	}
	if m.User.Bot {
		return nil
	}
	if err := s.MessageReactionRemove(a.ChannelID, a.MessageID, a.Emoji.APIName(), a.UserID); err != nil {
		h.log.WithError(err).Error("failed to remove emoji")
	}

	if !util.MemberHasRole(m, &discordgo.Role{ID: rid}) {
		return s.GuildMemberRoleAdd(a.GuildID, a.UserID, rid)
	}
	return s.GuildMemberRoleRemove(a.GuildID, a.UserID, rid)

}
func (h *Handler) Add() func(s *discordgo.Session, a *discordgo.MessageReactionAdd) {
	if !h.Enabled {
		return nil
	}
	return func(s *discordgo.Session, a *discordgo.MessageReactionAdd) {
		h := h
		if a.ChannelID != h.Channel {
			return
		}
		log := h.log.WithFields(logrus.Fields{"reaction": a})
		emid := a.Emoji.ID
		if emid == "" {
			emid = a.Emoji.Name
		}
		if id, ok := h.triggerMappings[emid]; ok {
			m, err := s.GuildMember(a.GuildID, a.UserID)
			if err != nil {
				h.log.WithError(err).Error("failed to fetch user to check mod status")
				return
			}
			if !config.MemberIsMod(m) {
				return
			}
			if err := h.setupMessage(s, a, id); err != nil {
				log.WithError(err).Error("failed to setup message")
			}
			return
		}

		if id, ok := h.SetMappings[a.MessageID]; ok {
			if err := h.handleMessageReact(s, a, id); err != nil {
				log.WithError(err).Error("failed to handle message react")
			}
		}
	}
}
func (h *Handler) Remove() func(s *discordgo.Session, r *discordgo.MessageReactionRemove) {
	// noop
	return nil
}
