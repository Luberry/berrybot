package reactions

import "github.com/bwmarrin/discordgo"

type Handler interface {
	Add() func(s *discordgo.Session, a *discordgo.MessageReactionAdd)
	Remove() func(s *discordgo.Session, r *discordgo.MessageReactionRemove)
}

func Register(s *discordgo.Session, h Handler) error {
	s.AddHandler(func(s *discordgo.Session, a *discordgo.MessageReactionAdd) {
		h := h
		if f := h.Add(); f != nil {
			f(s, a)
		}
	})
	s.AddHandler(func(s *discordgo.Session, r *discordgo.MessageReactionRemove) {
		h := h
		if f := h.Remove(); f != nil {
			f(s, r)
		}
	})
	return nil
}
