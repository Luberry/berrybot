package star_tracker

import (
	"context"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/karrick/tparse/v2"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/luberry/berrybot/internal/commands"
	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/reactions"
	"gitlab.com/luberry/berrybot/internal/util"
)

var handler config.Configureable = &Handler{}
var _ reactions.Handler = handler.(*Handler)
var _ commands.Handler = handler.(*Handler)

func init() {
	config.Register(handler)
}

type Handler struct {
	Enabled          bool          `yaml:"enabled"`
	PeriodStart      time.Time     `yaml:"periodStart"`
	Duration         time.Duration `yaml:"duration"`
	Current          starMap       `yaml:"current"`
	Previous         starMap       `yaml:"previous"`
	Emote            string        `yaml:"emoteID"`
	IgnoreModerators bool          `yaml:"ignoreModerators"`
	log              *logrus.Entry

	appInfo *app.Info
	m       sync.Mutex
}
type starConfig struct {
	ID    string         `yaml:"id"`
	Total int            `yaml:"total"`
	Posts map[string]int `yaml:"posts"`
}
type starMap map[string]*starConfig

const (
	cmdName      = "starTracker"
	stats        = "stats"
	duration     = "duration"
	value        = "set"
	previous     = "previous"
	limit        = "limit"
	disabled     = "disabled"
	defaultLimit = 10
)

func (h *Handler) Defaults() {
	h.Enabled = false
	h.PeriodStart = time.Time{}
	h.Duration = time.Duration(0)
	h.Current = nil
	h.Previous = nil
	h.IgnoreModerators = true
}
func (h *Handler) SetAppInfo(inf *app.Info) {
	h.appInfo = inf
}
func (h *Handler) rollover(now time.Time, dur time.Duration) error {
	h.m.Lock()
	defer h.m.Unlock()
	var changed bool
	if h.Duration != dur {
		changed = true
		h.Duration = dur
		if err := config.SaveConfig(h); err != nil {
			return err
		}
		if dur == 0 {
			return nil
		}
	}

	if !h.PeriodStart.IsZero() && !changed && h.PeriodStart.Add(h.Duration).After(now) {
		return nil
	}
	h.Previous = h.Current
	h.Current = make(starMap)
	h.PeriodStart = now
	return config.SaveConfig(h)
}
func (h *Handler) Commands(_ *discordgo.Session) []*discordgo.ApplicationCommand {
	return nil
}
func (h *Handler) Name() string {
	return cmdName
}
func (h *Handler) Init(ctx context.Context) error {
	h.log = logrus.WithField("name", h.Name())
	return nil
}
func (h *Handler) ModCommands(s *discordgo.Session) []*discordgo.ApplicationCommand {
	if !h.Enabled {
		return nil
	}
	return modCommands
}
func (h *Handler) Handler(_ string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	return nil
}
func (h *Handler) LegacyHandler(_ string) func(s *discordgo.Session, i *discordgo.MessageCreate) {
	return nil
}
func (h *Handler) stats(s *discordgo.Session, current bool, lim int) string {
	if lim < 1 {
		lim = defaultLimit
	}
	data := make(map[int][]string)
	c := "current"
	cur := h.Current
	if !current {
		c = "previous"
		cur = h.Previous
	}
	var keys []int
	for i, v := range cur {
		if _, ok := data[v.Total]; !ok {
			keys = append(keys, v.Total)
		}
		data[v.Total] = append(data[v.Total], i)
	}

	sort.Ints(keys)
	j := 0
	var b strings.Builder
	b.WriteString("***These are the ")
	b.WriteString(c)
	b.WriteString(" star counts:***")
	for i := len(keys) - 1; i > 0 && j < lim; i-- {
		k := keys[i]
		stars := make([]string, len(data[k]))
		copy(stars, data[k])
		sort.Strings(stars)
		for _, star := range stars {
			if j >= lim {
				break
			}

			name := ""
			if m, err := s.State.Member(h.appInfo.GuildID, star); err != nil {
				h.log.WithField("uid", star).WithError(err).Error("failed fetching users name, falling back to api call")
				u, err := s.GuildMember(h.appInfo.GuildID, star)
				if err != nil {
					h.log.WithField("uid", star).WithError(err).Error("failed fetching users name from api call, skipping")
					continue
				}
				name = u.User.Username
			} else {
				name = m.User.Username
			}
			b.WriteRune('\n')
			b.WriteString(strconv.Itoa(j + 1))
			b.WriteString(".) `")
			b.WriteString(name)
			b.WriteString("`: ")
			b.WriteString(strconv.Itoa(k))
			b.WriteString(" star")
			if k > 1 {
				b.WriteRune('s')
			}
			j++
		}
	}

	return b.String()
}
func (h *Handler) ModHandler(n string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	if !h.Enabled {
		return nil
	}
	switch n {
	case strings.ToLower(cmdName):
		return func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			h := h
			content := ""
			cmd := i.ApplicationCommandData().Options[0]
			switch cmd.Name {
			case stats:
				util.InteractionRespond(s, i.Interaction, "fetching stats")
				opts := cmd.Options
				if len(opts) == 0 {
					content = h.stats(s, true, defaultLimit)
					break
				}
				options := make(map[string]*discordgo.ApplicationCommandInteractionDataOption)
				for _, opt := range opts {
					options[opt.Name] = opt
				}

				content = h.stats(s,
					!util.GetInteractionOptBool(options, previous),
					util.GetInteractionOptInt(options, limit),
				)
			case duration:
				opts := cmd.Options
				if len(opts) == 0 {
					content = cmdName + "is disabled"
					if h.Duration > time.Duration(0) {
						content = fmt.Sprintf("starTracker is currently set to refresh every %s.", h.Duration.String())
					}
					if err := util.InteractionRespond(s, i.Interaction, content); err != nil {
						h.log.WithError(err).Error("failed to respond to duration check")
					}
					return
				}
				util.InteractionRespond(s, i.Interaction, "updating refresh period for star tracker")
				options := make(map[string]*discordgo.ApplicationCommandInteractionDataOption)
				for _, opt := range opts {
					options[opt.Name] = opt
				}

				d := util.GetInteractionOptString(options, value)
				if strings.ToLower(d) == disabled {
					content = "disabled star tracker"
					if err := h.rollover(time.Now(), time.Duration(0)); err != nil {
						content = errors.Wrap(err, "failed to disable starTracker").Error()
					}
					break
				}
				dur, err := tparse.AbsoluteDuration(time.Now(), d)
				if err != nil {
					content = errors.Wrap(err, "failed update startracker refresh duration").Error()
					break
				}
				if err := h.rollover(time.Now(), dur); err != nil {
					content = err.Error()
					break
				}

				content = fmt.Sprintf("updated starTracker duration to %s", dur.String())

			default:
				content = fmt.Sprintf("invalid subcommand `%s` for `%s`, valid subcommands are [`%s`,`%s`]", cmd.Name, cmdName, stats, duration)
			}
			if _, err := s.InteractionResponseEdit(i.Interaction, &discordgo.WebhookEdit{Content: util.StringPTR(content)}); err != nil {
				h.log.WithError(err).WithField("interaction", i).Error("failed to send response")
			}
		}
	}
	return nil
}
func (h *Handler) handle(add bool) func(s *discordgo.Session, a *discordgo.MessageReaction) {
	if !h.Enabled {
		return nil
	}

	return func(s *discordgo.Session, a *discordgo.MessageReaction) {
		log := h.log.WithField("reaction", a)
		if a.Emoji.ID != h.Emote {
			return
		}
		msg, err := s.ChannelMessage(a.ChannelID, a.MessageID)
		if err != nil {
			log.WithError(err).Error("could not fetch message to determine author")
			return
		}
		log.WithField("msg", msg).Info()

		if msg.Author.ID == a.UserID {
			return
		}
		if h.IgnoreModerators {
			u, err := s.GuildMember(a.GuildID, msg.Author.ID)
			if err != nil {
				log.WithError(err).Error("assuming member is not a mod")
			} else if config.MemberIsMod(u) {
				return
			}
		}

		now := time.Now()
		if err := h.rollover(now, h.Duration); err != nil {
			log.WithError(err).Error("could not rollover time")
			return
		}

		aid := msg.Author.ID

		_, ok := h.Current[aid]
		if !ok {
			if !add {
				return
			}
			h.Current[aid] = &starConfig{
				ID:    aid,
				Total: 0,
				Posts: make(map[string]int),
			}
		}
		defer func() {
			if err := config.SaveConfig(h); err != nil {
				log.WithError(err).Error("failed to save reaction star")
			}
		}()
		h.m.Lock()
		defer h.m.Unlock()
		if h.Current[aid].Posts == nil {
			h.Current[aid].Posts = make(map[string]int)
		}
		if !add {
			h.Current[aid].Total--
			if h.Current[aid].Total < 0 {
				h.Current[aid].Total = 0
			}
			if h.Current[aid].Posts[a.MessageID] == 0 {
				return
			}
			h.Current[aid].Posts[a.MessageID]--
			if h.Current[aid].Total <= 0 {
				delete(h.Current, aid)
			}
			return
		}
		h.Current[aid].Total++
		h.Current[aid].Posts[a.MessageID]++

	}
}

func (h *Handler) Add() func(s *discordgo.Session, a *discordgo.MessageReactionAdd) {
	return func(s *discordgo.Session, a *discordgo.MessageReactionAdd) {
		if f := h.handle(true); f != nil {
			f(s, a.MessageReaction)
		}
	}
}
func (h *Handler) Remove() func(s *discordgo.Session, r *discordgo.MessageReactionRemove) {
	return func(s *discordgo.Session, a *discordgo.MessageReactionRemove) {
		if f := h.handle(false); f != nil {
			f(s, a.MessageReaction)
		}
	}
}

var (
	modCommands = []*discordgo.ApplicationCommand{
		{
			Name:        strings.ToLower(cmdName),
			Description: "manage star tracker settings",

			Options: []*discordgo.ApplicationCommandOption{
				{
					Name:        stats,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "fetch the list of stars",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        previous,
							Description: "show previous set of stars",
							Type:        discordgo.ApplicationCommandOptionBoolean,
							Required:    false,
						},
						{
							Name:        limit,
							Description: "description of the user command",
							Type:        discordgo.ApplicationCommandOptionInteger,
							Required:    false,
						},
					},
				},
				{
					Name:        duration,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "get or set the current duration for startracker",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        value,
							Description: "length of time to calculate stars, enter `disabled` to disable ",
							Type:        discordgo.ApplicationCommandOptionString,
							Required:    false,
						},
					},
				},
			},
		},
	}
)
