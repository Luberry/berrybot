package user

import (
	"context"
	"errors"
	"fmt"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/luberry/berrybot/internal/commands"
	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/util"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

var handler config.Configureable = &Handler{}
var _ commands.Handler = handler.(*Handler)

const destDMS = "dms"

type userCommand struct {
	idx         int
	Body        string
	Description string
	Count       int
	Destination string
	Children    map[string]*userCommand
	m           sync.Mutex
}

func init() {
	config.Register(handler)
}
func newUC(description, body string, channel *discordgo.Channel, dmsOnly bool) (*userCommand, error) {
	uc := &userCommand{
		Children: make(map[string]*userCommand),
	}
	if err := uc.update(description, body, channel, dmsOnly); err != nil {
		return nil, err
	}
	uc.idx = -1
	return uc, nil
}
func (u *userCommand) counts(parent, child string, lvl int, counts map[int][]string) {
	if lvl >= 3 {
		return
	}
	if len(u.Children) > 0 {
		for n := range u.Children {
			p := child
			if lvl > 0 {
				p = fmt.Sprintf("%s.%s", parent, child)
			}
			u.Children[n].counts(p, n, lvl+1, counts)
		}
		return
	}
	if u.Count > 0 {
		n := child
		if lvl > 0 {
			n = fmt.Sprintf("%s.%s", parent, child)
		}
		counts[u.Count] = append(counts[u.Count], n)
		return
	}
}

func (u *userCommand) update(description, body string, channel *discordgo.Channel, dmsOnly bool) error {
	if description != "" {
		u.Description = description
	}
	if body != "" {
		u.Body = body
	}
	if channel != nil {
		u.Destination = channel.ID
	}
	if dmsOnly {
		u.Destination = destDMS
	}
	return nil
}
func (u *userCommand) applicationCommand(n string) *discordgo.ApplicationCommand {
	ac := &discordgo.ApplicationCommand{
		Name:        n,
		Description: u.Description,
		Type:        discordgo.ChatApplicationCommand,
	}
	for nme, child := range u.Children {
		if opt := child.applicationCommandOpt(nme, 1); opt != nil {
			ac.Options = append(ac.Options, opt)
		}
	}
	return ac
}
func (u *userCommand) applicationCommandOpt(n string, lvl int) *discordgo.ApplicationCommandOption {
	if lvl > 2 {
		logrus.WithField("name", n).WithField("level", lvl).Warn("nested commands can only be nested two layers deep")
		return nil
	}
	opt := &discordgo.ApplicationCommandOption{
		Name:        n,
		Description: u.Description,
		Type:        discordgo.ApplicationCommandOptionSubCommand,
	}

	for nme, child := range u.Children {
		if o := child.applicationCommandOpt(nme, lvl+1); o != nil {
			opt.Options = append(opt.Options, o)
		}
	}

	if len(opt.Options) > 0 {
		opt.Type = discordgo.ApplicationCommandOptionSubCommandGroup
	}
	return opt
}
func invalidCommand(s *discordgo.Session, i *discordgo.Interaction) {
	if err := util.InteractionRespond(s, i, "invalid command or option, please try again"); err != nil {
		logrus.WithError(err).WithField("interaction", i).Error("failed to get send response")
	}
}

func (u *userCommand) legacyHandler(h *Handler) func(s *discordgo.Session, m *discordgo.MessageCreate) {
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		data := strings.Split(m.Message.Content[1:], " ")
		// For now hidden commands cannot have args
		if len(data) != 1 {
			return
		}
		if _, err := s.ChannelMessageSend(m.ChannelID, u.Body); err != nil {
			logrus.WithField("messageCreate", m).WithError(err).Error("could not send legacy hidden command")
			return
		}
		h.HiddenCommands[data[0]].m.Lock()
		defer h.HiddenCommands[data[0]].m.Unlock()
		h.HiddenCommands[data[0]].Count++
		config.SaveConfig(h)

	}

}

func (u *userCommand) applicationCommandHandler(n string, h *Handler) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	return func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		if i.Type != discordgo.InteractionApplicationCommand {
			invalidCommand(s, i.Interaction)
			return
		}
		ac := i.ApplicationCommandData()
		if ac.Name != n {
			invalidCommand(s, i.Interaction)
			return
		}
		if len(ac.Options) == 0 {
			u.respond(s, i.Interaction, h.appInfo.AppID, n)
			h.VisibleCommands[n].m.Lock()
			defer h.VisibleCommands[n].m.Unlock()
			h.VisibleCommands[n].Count++
			config.SaveConfig(h)
			return
		}
		if len(ac.Options) != 1 {
			invalidCommand(s, i.Interaction)
			return
		}
		cmd := ac.Options[0].Name
		c, ok := u.Children[cmd]
		if !ok {
			invalidCommand(s, i.Interaction)
			return
		}
		switch ac.Options[0].Type {
		case discordgo.ApplicationCommandOptionSubCommand:
			c.respond(s, i.Interaction, h.appInfo.AppID, cmd)
			h.VisibleCommands[n].Children[cmd].m.Lock()
			defer h.VisibleCommands[n].Children[cmd].m.Unlock()
			h.VisibleCommands[n].Children[cmd].Count++
			config.SaveConfig(h)
			return
		case discordgo.ApplicationCommandOptionSubCommandGroup:
			aco := ac.Options[0]
			if len(aco.Options) != 1 {
				invalidCommand(s, i.Interaction)
				return
			}
			if aco.Type != discordgo.ApplicationCommandOptionSubCommand && aco.Type != discordgo.ApplicationCommandOptionSubCommandGroup {
				invalidCommand(s, i.Interaction)
				return
			}
			cmd2 := aco.Options[0].Name
			cn, ok := c.Children[cmd2]
			if !ok {
				invalidCommand(s, i.Interaction)
				return
			}
			cn.respond(s, i.Interaction, h.appInfo.AppID, cmd2)
			h.VisibleCommands[n].Children[cmd].Children[cmd2].m.Lock()
			defer h.VisibleCommands[n].Children[cmd].Children[cmd2].m.Unlock()
			h.VisibleCommands[n].Children[cmd].Children[cmd2].Count++
			config.SaveConfig(h)
			return
		}
		invalidCommand(s, i.Interaction)
	}
}

func (u *userCommand) respond(s *discordgo.Session, i *discordgo.Interaction, appID, n string) {
	warn := true
	content := u.Body
	if content == "" {
		content = "no content"
	}
	dest := u.Destination
	if n == "help" {
		warn = false
	}
	if config.MemberIsMod(i.Member) || dest == "" || i.GuildID == "" || (dest != destDMS && i.ChannelID == dest) {
		if err := util.InteractionRespond(s, i, content); err != nil {
			logrus.WithError(err).WithField("interaction", i).Error("failed to get send response")
		}
		return
	}
	if dest == destDMS {
		util.InteractionRespond(s, i, "attempting to redirect to dms")
		msg := content
		if warn {
			msg = fmt.Sprintf("*%s please run this in your dms next time.*\n%s", i.Member.Mention(), content)
		}
		var err error
		var c *discordgo.Channel
		authorId := i.Member.User.ID
		log := logrus.WithField("author", authorId).WithError(err)
		if c, err = s.UserChannelCreate(authorId); err != nil {
			log.Error("could not create dm to talk to member")
			s.InteractionResponseEdit(i, &discordgo.WebhookEdit{
				Content: util.StringPTR(msg),
			})
			return
		}

		if _, err := s.ChannelMessageSend(c.ID, msg); err != nil {
			log.WithError(err).Error("could not send dm to member")
			s.InteractionResponseEdit(i, &discordgo.WebhookEdit{
				Content: util.StringPTR(msg),
			})
			return
		}
		s.InteractionResponseDelete(i)
		return
	}
	c := &discordgo.Channel{ID: dest}
	util.InteractionRespond(s, i, "attempting to redirect to "+c.Mention())
	msg := content
	if warn {
		msg = fmt.Sprintf("*%s please run this in %s next time.*\n%s", i.Member.Mention(), c.Mention(), content)
	}
	if _, err := s.ChannelMessageSend(dest, msg); err != nil {
		logrus.WithError(err).Error("could not send message to channel")
		s.InteractionResponseEdit(i, &discordgo.WebhookEdit{
			Content: util.StringPTR(msg),
		})
		return
	}
	s.InteractionResponseDelete(i)
}

type Handler struct {
	vcm             sync.Mutex
	VisibleCommands map[string]*userCommand `yaml:"commands"`
	appCMDs         []*discordgo.ApplicationCommand
	HiddenCommands  map[string]*userCommand `yaml:"hiddenCommands"`
	hcm             sync.Mutex
	Enabled         bool
	log             *logrus.Entry
	appInfo         *app.Info
}

func (h *Handler) Name() string {
	return "userCommands"
}

func (h *Handler) Init(ctx context.Context) error {
	h.log = logrus.WithField("name", h.Name())
	if !h.Enabled {
		return nil
	}
	for n, uc := range h.VisibleCommands {
		uc.idx = len(h.appCMDs)
		h.appCMDs = append(h.appCMDs, uc.applicationCommand(n))
	}

	return nil
}
func (h *Handler) Defaults() {
	h.Enabled = true
	h.VisibleCommands = nil
	h.HiddenCommands = nil
}

func (h *Handler) Commands(s *discordgo.Session) []*discordgo.ApplicationCommand {
	if !h.Enabled {
		return nil
	}
	return h.appCMDs
}

func (h *Handler) removeLegacyCMD(s *discordgo.Session, n string) error {
	h.hcm.Lock()
	defer h.hcm.Unlock()
	if _, ok := h.HiddenCommands[n]; !ok {
		return errors.New("command not found")
	}
	delete(h.HiddenCommands, n)
	return nil
}
func (h *Handler) removeAppCMD(s *discordgo.Session, n string) error {
	h.vcm.Lock()
	defer h.vcm.Unlock()
	lvls := strings.Split(n, ".")
	found := false
	switch len(lvls) {
	case 1:
		delete(h.VisibleCommands, lvls[0])
		found = true
	case 2:
		if _, ok := h.VisibleCommands[lvls[0]]; ok {
			delete(h.VisibleCommands[lvls[0]].Children, lvls[1])
			found = true
		}
	case 3:
		if _, ok := h.VisibleCommands[lvls[0]]; ok {
			if _, ok := h.VisibleCommands[lvls[0]].Children[lvls[1]]; ok {
				delete(h.VisibleCommands[lvls[0]].Children[lvls[1]].Children, lvls[2])
				found = true
			}
		}
	}
	if !found {
		return errors.New("command not found")
	}

	return commands.RemoveCommand(s, h.appInfo.AppID, h.appInfo.GuildID, h.appInfo.GuildCommands, n)
}

var (
	nameRexp    = regexp.MustCompile(`^[\w-]{1,32}$`)
	modCommands = []*discordgo.ApplicationCommand{
		{
			Name:        userCmd,
			Description: "add or remove custom commands for users, the content will be taken from your previous message",

			Options: []*discordgo.ApplicationCommandOption{
				{
					Name:        set,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "create or edit a user command",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        name,
							Description: "cmd name including sub cmds i.e `this.command.set` would be `/this command set`",
							Type:        discordgo.ApplicationCommandOptionString,
							Required:    true,
						},
						{
							Name:        desc,
							Description: "description of the user command",
							Type:        discordgo.ApplicationCommandOptionString,
							Required:    false,
						},
						{
							Name:        chn,
							Description: "channel to redirect the command to",
							Type:        discordgo.ApplicationCommandOptionChannel,
							Required:    false,
						},
						{
							Name:        dms,
							Description: "redirect to dms (overrides channel if set)",
							Type:        discordgo.ApplicationCommandOptionBoolean,
							Required:    false,
						},
						{
							Name:        last,
							Description: "uses your last message if set for the cmd, default if new cmd",
							Type:        discordgo.ApplicationCommandOptionBoolean,
							Required:    false,
						},
					},
				},
				{
					Name:        sethidden,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "create or edit a hidden command",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        name,
							Description: "hidden command name",
							Type:        discordgo.ApplicationCommandOptionString,
							Required:    true,
						},
						{
							Name:        desc,
							Description: "description of the hidden command",
							Type:        discordgo.ApplicationCommandOptionString,
							Required:    false,
						},
						{
							Name:        last,
							Description: "uses your last message if set for the cmd, default if new cmd",
							Type:        discordgo.ApplicationCommandOptionBoolean,
							Required:    false,
						},
					},
				},
				{
					Name:        remove,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "remove a user command, will delete all subcommands if the parent is removed",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        name,
							Description: "command name, see `set` for details",
							Type:        discordgo.ApplicationCommandOptionString,
							Required:    true,
						},
					},
				},
				{
					Name:        removehidden,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "remove a hidden command",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Name:        name,
							Description: "command name",
							Type:        discordgo.ApplicationCommandOptionString,
							Required:    true,
						},
					},
				},
				{
					Name:        hidden,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "shows a list of hidden commands",
				},
				{
					Name:        counts,
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Description: "shows usage counts of user commands",
				},
			},
		},
	}
)

func (h *Handler) getLastMessage(s *discordgo.Session, i *discordgo.Interaction) (*discordgo.Message, error) {
	messages, err := s.ChannelMessages(i.ChannelID, 10, "", "", "")
	if err != nil {
		return nil, err
	}
	var uid string
	if i.User != nil {
		uid = i.User.ID
	} else {
		uid = i.Member.User.ID
	}
	for _, msg := range messages {
		if msg.Author.ID == uid {
			return msg, nil
		}
	}
	return nil, errors.New("last message not found")
}
func (h *Handler) createLegacyCommand(s *discordgo.Session, i *discordgo.Interaction, n, description string, lastMsg *bool) error {
	h.hcm.Lock()
	defer h.hcm.Unlock()
	isNew := false
	cmd, ok := h.HiddenCommands[n]
	if !ok {
		if lastMsg == nil {
			val := true
			lastMsg = &val
		}
		isNew = true
	}
	var body string
	if lastMsg != nil && *lastMsg {
		m, err := h.getLastMessage(s, i)
		if err != nil {
			return err
		}
		body = m.Content
		if len(body) == 0 {
			if len(m.Attachments) <= 0 {
				return errors.New("no body or attachments")
			}
			body = m.Attachments[0].URL
		}

	}
	var err error
	if isNew {
		if cmd, err = newUC(description, body, nil, false); err != nil {
			return err
		}
	} else if err = cmd.update(description, body, nil, false); err != nil {
		return err
	}
	h.HiddenCommands[n] = cmd
	return config.SaveConfig(h)
}
func (h *Handler) commandUsage() string {
	counts := make(map[int][]string)
	for n, cmd := range h.VisibleCommands {
		cmd.counts("", "/"+n, 0, counts)
	}
	for n, cmd := range h.HiddenCommands {
		cmd.counts("", "!"+n, 0, counts)
	}
	if len(counts) == 0 {
		return "no commands have been used"
	}
	var b strings.Builder
	b.WriteString("__**Command usage stats:**__")
	keys := make([]int, 0, len(counts))
	for k := range counts {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	for idx := len(keys) - 1; idx >= 0; idx-- {
		cmd := counts[keys[idx]]
		sort.Strings(cmd)
		for _, n := range cmd {
			b.WriteRune('\n')
			b.WriteRune('`')
			b.WriteString(n)
			b.WriteString("`: ")
			b.WriteString(strconv.Itoa(keys[idx]))
		}
	}
	return b.String()
}
func (h *Handler) createAppCommand(s *discordgo.Session, i *discordgo.Interaction, n, description string, channel *discordgo.Channel, dmsOnly bool, lastMsg *bool) error {
	h.hcm.Lock()
	defer h.hcm.Unlock()
	lvls := strings.Split(n, ".")
	if len(lvls) < 1 {
		return errors.New("incorrect value passed for name")
	}
	for _, lvl := range lvls {
		if !nameRexp.MatchString(lvl) {
			return errors.New("invalid characters in command name. must follow the regexp `^[\\w-]{1,32}$`")
		}
	}
	isNew := false
	cmd, ok := h.VisibleCommands[lvls[0]]
	if !ok {
		if lastMsg == nil {
			val := true
			lastMsg = &val
		}
		isNew = true
	}
	var err error
	if isNew && len(lvls) != 1 {
		if cmd, err = newUC(lvls[0], "", nil, false); err != nil {
			return err
		}
		h.VisibleCommands[lvls[0]] = cmd
	}
	if isNew && description == "" {
		return errors.New("must supply a description for a new command")
	}

	var body string
	if lastMsg != nil && *lastMsg {
		m, err := h.getLastMessage(s, i)
		if err != nil {
			return err
		}
		body = m.Content
		if len(body) == 0 {
			if len(m.Attachments) <= 0 {
				return errors.New("no body or attachments")
			}
			body = m.Attachments[0].URL
		}
	}

	switch len(lvls) {
	case 1:
		if isNew {
			if cmd, err = newUC(description, body, channel, dmsOnly); err != nil {
				return err
			}
		} else if err = cmd.update(description, body, channel, dmsOnly); err != nil {
			return err
		}
	case 2:
		if c, ok := cmd.Children[lvls[1]]; !ok {
			if lastMsg == nil || *lastMsg {
				m, err := h.getLastMessage(s, i)
				if err != nil {
					return err
				}
				body = m.Content
			}
			nc, err := newUC(description, body, channel, dmsOnly)
			if err != nil {
				return err
			}
			cmd.Children[lvls[1]] = nc
		} else {
			c.update(description, body, channel, dmsOnly)
		}
	case 3:
		c, ok := cmd.Children[lvls[1]]
		if !ok {
			if c, err = newUC(lvls[1], "", nil, false); err != nil {
				return err
			}
			cmd.Children[lvls[1]] = c
		}
		if cn, ok := c.Children[lvls[2]]; !ok {
			if lastMsg == nil || *lastMsg {
				m, err := h.getLastMessage(s, i)
				if err != nil {
					return err
				}
				body = m.Content
			}
			nc, err := newUC(description, body, channel, dmsOnly)
			if err != nil {
				return err
			}
			cmd.Children[lvls[1]].Children[lvls[2]] = nc
		} else {
			cn.update(description, body, channel, dmsOnly)
		}
	}
	if cmd.idx == -1 {
		cmd.idx = len(h.appCMDs)
		h.appCMDs = append(h.appCMDs, cmd.applicationCommand(lvls[0]))
	} else {
		h.appCMDs[cmd.idx] = cmd.applicationCommand(lvls[0])
	}
	h.VisibleCommands[lvls[0]] = cmd
	gid := ""
	if h.appInfo.GuildCommands {
		gid = h.appInfo.GuildID
	}
	if err := commands.AddCommand(s, h.appInfo.AppID, gid, h.appCMDs[cmd.idx]); err != nil {
		return err
	}
	return config.SaveConfig(h)
}

func (h *Handler) ModHandler(n string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	if !h.Enabled {
		return nil
	}
	switch n {
	case userCmd:
		return func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			h := h
			content := ""
			if i.Type != discordgo.InteractionApplicationCommand {
				return
			}
			cmd := i.ApplicationCommandData().Options[0]
			switch cmd.Name {
			case set:
				util.InteractionRespond(s, i.Interaction, "creating or editing command")
				opts := cmd.Options
				if len(opts) < 1 {
					content = fmt.Sprintf("invalid opts for `%s %s`, must provide the name of the command to %s ", userCmd, set, set)
					break
				}
				if opts[0].Name != name {
					content = fmt.Sprintf("invalid opt `%s` for `%s %s`, must provide the name of the command to %s ", opts[0].Name, userCmd, set, set)
					break
				}
				add := opts[0].StringValue()
				if add == "" {
					content = fmt.Sprintf("invalid name for `%s %s`, must provide a valid name of the command to %s ", userCmd, set, set)
					break
				}
				if _, err := s.InteractionResponseEdit(i.Interaction, &discordgo.WebhookEdit{Content: util.StringPTR(fmt.Sprintf("creating command `%s`", add))}); err != nil {
					h.log.WithError(err).WithField("interaction", i).Error("failed to send response")
				}
				options := make(map[string]*discordgo.ApplicationCommandInteractionDataOption)
				for _, opt := range opts[1:] {
					options[opt.Name] = opt
				}

				description := util.GetInteractionOptString(options, desc)
				channel := util.GetInteractionOptChannel(s, options, chn)
				dmsOnly := util.GetInteractionOptBool(options, dms)
				var lastMsg *bool
				if v, ok := options[last]; ok {
					val := v.BoolValue()
					lastMsg = &val
				}
				if err := h.createAppCommand(s, i.Interaction, add, description, channel, dmsOnly, lastMsg); err != nil {
					content = err.Error()
					break
				}

				content = "created command"
			case sethidden:
				util.InteractionRespond(s, i.Interaction, "creating or editing command")
				opts := cmd.Options
				if len(opts) < 1 {
					content = fmt.Sprintf("invalid opts for `%s %s`, must provide the name of the command to %s ", userCmd, set, set)
					break
				}
				if opts[0].Name != name {
					content = fmt.Sprintf("invalid opt `%s` for `%s %s`, must provide the name of the command to %s ", opts[0].Name, userCmd, set, set)
					break
				}
				add := opts[0].StringValue()
				if add == "" {
					content = fmt.Sprintf("invalid name for `%s %s`, must provide a valid name of the command to %s ", userCmd, set, set)
					break
				}
				if _, err := s.InteractionResponseEdit(i.Interaction, &discordgo.WebhookEdit{Content: util.StringPTR(fmt.Sprintf("creating hidden command `%s`", add))}); err != nil {
					h.log.WithError(err).WithField("interaction", i).Error("failed to send response")
				}
				options := make(map[string]*discordgo.ApplicationCommandInteractionDataOption)
				for _, opt := range opts[1:] {
					options[opt.Name] = opt
				}

				description := util.GetInteractionOptString(options, desc)
				var lastMsg *bool
				if v, ok := options[last]; ok {
					val := v.BoolValue()
					lastMsg = &val
				}

				if err := h.createLegacyCommand(s, i.Interaction, add, description, lastMsg); err != nil {
					content = err.Error()
					break
				}
				content = "created hidden command"

			case remove, removehidden:
				util.InteractionRespond(s, i.Interaction, "removing command")
				opts := cmd.Options
				if len(opts) != 1 {
					content = fmt.Sprintf("invalid opts for `%s %s`, must provide the name of the command to %s ", userCmd, n, n)
					break
				}
				if opts[0].Name != name {
					content = fmt.Sprintf("invalid opt `%s` for `%s %s`, must provide the name of the command to %s ", opts[0].Name, userCmd, n, n)
					break
				}
				rm := opts[0].StringValue()
				if _, err := s.InteractionResponseEdit(i.Interaction, &discordgo.WebhookEdit{Content: util.StringPTR(fmt.Sprintf("removing command `%s` and all of its subcommands", rm))}); err != nil {
					h.log.WithError(err).WithField("interaction", i).Error("failed to send response")
				}
				if cmd.Name == removehidden {
					if err := h.removeLegacyCMD(s, rm); err != nil {
						content = err.Error()
						break
					}

				} else if err := h.removeAppCMD(s, rm); err != nil {
					content = err.Error()
					break
				}
				if err := config.SaveConfig(h); err != nil {
					content = err.Error()
					break
				}
				content = fmt.Sprintf("successfully removed command `%s` and all of its subcommands", rm)
			case hidden:
				util.InteractionRespond(s, i.Interaction, "fetching hidden command list")
				var b strings.Builder
				b.WriteString("__**Hidden Commands:**__")
				for n, cmd := range h.HiddenCommands {
					b.WriteString("\n`!")
					b.WriteString(n)
					b.WriteRune('`')
					if cmd.Description != "" {
						b.WriteString(" - ")
						b.WriteString(cmd.Description)
					}
				}
				content = b.String()
			case counts:
				util.InteractionRespond(s, i.Interaction, "fetching command usage stats")
				content = h.commandUsage()
			default:
				content = fmt.Sprintf("invalid subcommand `%s` for `%s`, valid subcommands are [`%s`,`%s`]", cmd.Name, userCmd, set, remove)
			}
			if _, err := s.InteractionResponseEdit(i.Interaction, &discordgo.WebhookEdit{Content: util.StringPTR(content)}); err != nil {
				h.log.WithError(err).WithField("interaction", i).Error("failed to send response")
			}
		}
	}
	return nil
}

const (
	userCmd      = "user-cmd"
	set          = "set"
	name         = "name"
	desc         = "description"
	chn          = "channel"
	dms          = "dms"
	last         = "use-last-msg"
	remove       = "remove"
	hidden       = "hidden"
	sethidden    = "set-hidden"
	removehidden = "remove-hidden"
	counts       = "counts"
)

func (h *Handler) ModCommands(s *discordgo.Session) []*discordgo.ApplicationCommand {
	if !h.Enabled {
		h.log.Debug("disabled")
		return nil
	}

	return modCommands
}

func (h *Handler) Handler(n string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	if uc, ok := h.VisibleCommands[n]; ok {
		hd := uc.applicationCommandHandler(n, h)

		return hd

	}
	return nil
}
func (h *Handler) LegacyHandler(n string) func(s *discordgo.Session, m *discordgo.MessageCreate) {
	if uc, ok := h.HiddenCommands[n]; ok {
		return uc.legacyHandler(h)
	}
	return nil
}

func (h *Handler) SetAppInfo(inf *app.Info) {
	h.appInfo = inf
}
