package contribute

import (
	"context"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gitlab.com/luberry/berrybot/internal/commands"
	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/util"
)

var handler config.Configureable = &Handler{}
var _ commands.Handler = handler.(*Handler)

func init() {
	config.Register(handler)
}

type Handler struct {
	Enabled bool          `yaml:"enabled"`
	Role    string        `yaml:"role"`
	Channel string        `yaml:"channel"`
	Timeout time.Duration `yaml:"timeout"`
	log     *logrus.Entry

	appInfo *app.Info
}

const (
	contribute     = "contribute"
	defaultTimeout = 5 * time.Minute
)

func (h *Handler) Defaults() {
	h.Enabled = false
	h.Channel = ""
	h.Role = ""
	h.Timeout = defaultTimeout
}
func (h *Handler) SetAppInfo(inf *app.Info) {
	h.appInfo = inf
}
func (h *Handler) Commands(s *discordgo.Session) []*discordgo.ApplicationCommand {
	if !h.Enabled {
		return nil
	}
	return []*discordgo.ApplicationCommand{
		{
			Name:        contribute,
			Description: "run this to contribute one message to the resources channel",
		},
	}
}
func (h *Handler) Name() string {
	return "contribute"
}
func (h *Handler) Init(ctx context.Context) error {
	h.log = logrus.WithField("name", h.Name())
	return nil
}
func (h *Handler) ModCommands(_ *discordgo.Session) []*discordgo.ApplicationCommand {
	return nil
}
func (h *Handler) Handler(n string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	if !h.Enabled {
		return nil
	}
	switch n {
	case contribute:
		return func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			content := fmt.Sprintf("%s, you are now a contributor. You may now post **ONE** message to <#%s>, Thank you for your contribution!", i.Member.Mention(), h.Channel)
			if err := s.GuildMemberRoleAdd(i.GuildID, i.Member.User.ID, h.Role); err != nil {
				content = err.Error()
			}
			util.InteractionRespond(s, i.Interaction, content)
			time.Sleep(h.Timeout)
			if err := s.GuildMemberRoleRemove(i.GuildID, i.Member.User.ID, h.Role); err != nil {
				h.log.WithError(err).Error("could not remove contributor role")
			}
		}
	}
	return nil
}
func (h *Handler) LegacyHandler(_ string) func(s *discordgo.Session, i *discordgo.MessageCreate) {
	return nil
}
func (h *Handler) ModHandler(n string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	return nil
}
