package commands

import (
	"errors"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/hashicorp/go-multierror"
	"github.com/sirupsen/logrus"
	"gitlab.com/luberry/berrybot/internal/config/app"
)

type Handler interface {
	Commands(s *discordgo.Session) []*discordgo.ApplicationCommand
	Handler(name string) func(s *discordgo.Session, i *discordgo.InteractionCreate)
	ModCommands(s *discordgo.Session) []*discordgo.ApplicationCommand
	ModHandler(name string) func(s *discordgo.Session, i *discordgo.InteractionCreate)
	LegacyHandler(cmd string) func(s *discordgo.Session, m *discordgo.MessageCreate)
}

func AddCommand(s *discordgo.Session, appID, guildID string, cmd *discordgo.ApplicationCommand) error {
	_, err := s.ApplicationCommandCreate(appID, guildID, cmd)
	return err
}

func GetCommand(s *discordgo.Session, appID, guildID string, name string) (*discordgo.ApplicationCommand, error) {
	cmds, err := s.ApplicationCommands(appID, guildID)
	if err != nil {
		return nil, err
	}
	for _, cmd := range cmds {
		if cmd.Name == name {
			return cmd, nil
		}
	}
	return nil, errors.New("cmd not found")
}

func RemoveCommand(s *discordgo.Session, appID, guildID string, guildCommands bool, name string) error {
	gid := ""
	if guildCommands {
		gid = guildID
	}
	name = strings.TrimSpace(name)
	if name == "" {
		return errors.New("command name to remove is empty")
	}

	cmds, err := s.ApplicationCommands(appID, gid)
	if err != nil {
		return err
	}
	lvls := strings.Split(name, ".")
	for _, cmd := range cmds {
		if cmd.Name == lvls[0] {
			if len(lvls) == 1 {
				return s.ApplicationCommandDelete(appID, gid, cmd.ID)
			}
			if cmd.Options, err = RemoveCMDLevel(1, lvls, cmd.Options); err != nil {
				return err
			}

			_, err := s.ApplicationCommandCreate(appID, gid, cmd)
			if err != nil {
				logrus.WithError(err).WithField("cmd", cmd).Error()
				return err
			}
			return nil
		}
	}
	return errors.New("command not found")
}

func RemoveCMDLevel(idx int, lvls []string, opts []*discordgo.ApplicationCommandOption) ([]*discordgo.ApplicationCommandOption, error) {
	if idx == 3 || len(lvls) == idx {
		return opts, nil
	}
	for i, cmd := range opts {
		if cmd.Name == lvls[idx] {
			if len(lvls) == idx+1 {
				return append(opts[:i], opts[i+1:]...), nil
			}
			var err error
			opts[i].Options, err = RemoveCMDLevel(idx+1, lvls, cmd.Options)
			if len(opts[i].Options) == 0 {
				opts[i].Type = discordgo.ApplicationCommandOptionSubCommand
			}
			return opts, err
		}
	}
	return opts, errors.New("no subcommand to delete")
}

func CleanCommands(s *discordgo.Session, appID, guildID string) error {
	errs := &multierror.Error{}
	global, err := s.ApplicationCommands(appID, "")
	if err != nil {
		errs = multierror.Append(errs, err)
	}
	for _, cmd := range global {
		s.ApplicationCommandDelete(appID, "", cmd.ID)
	}
	guild, err := s.ApplicationCommands(appID, guildID)
	if err != nil {
		errs = multierror.Append(errs, err)
	}
	for _, cmd := range guild {
		s.ApplicationCommandDelete(appID, "", cmd.ID)
	}
	return errs.ErrorOrNil()
}

func Register(s *discordgo.Session, inf *app.Info, moderation *app.Moderation, h Handler) error {
	errs := &multierror.Error{}
	for _, cmd := range h.Commands(s) {
		logrus.WithField("cmd", cmd).Debug("registering user command")
		gid := ""
		if inf.GuildCommands {
			gid = inf.GuildID
		}
		errs = multierror.Append(AddCommand(s, inf.AppID, gid, cmd))
	}
	if err := errs.ErrorOrNil(); err != nil {
		return err
	}
	var modperms discordgo.ApplicationCommandPermissionsList
	for _, role := range moderation.Roles {
		modperms.Permissions = append(modperms.Permissions,
			&discordgo.ApplicationCommandPermissions{
				ID:         role,
				Type:       discordgo.ApplicationCommandPermissionTypeRole,
				Permission: true,
			})
	}
	for _, cmd := range h.ModCommands(s) {
		df := false
		cmd.DefaultPermission = &df
		logrus.WithField("cmd", cmd).Debug("registering mod command")
		gid := ""
		if moderation.GuildCommands {
			gid = inf.GuildID
		}
		var perms int64 = discordgo.PermissionBanMembers
		cmd.DefaultMemberPermissions = &perms

		cmd, err := s.ApplicationCommandCreate(inf.AppID, gid, cmd)
		if err != nil {
			errs = multierror.Append(errs, err)
			continue
		}
		logrus.WithField("cmd", cmd).Debug("return")

	}
	if err := errs.ErrorOrNil(); err != nil {
		logrus.Debug("permissions...")
		return err
	}
	s.AddHandler(func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		h := h
		if i.Type != discordgo.InteractionApplicationCommand {
			return
		}
		name := i.ApplicationCommandData().Name
		if h := h.Handler(name); h != nil {
			h(s, i)
			return
		}
		if h := h.ModHandler(name); h != nil {
			h(s, i)
			return
		}
	})

	s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if !strings.HasPrefix(m.Content, "!") || len(m.Content) == 1 {
			return
		}

		if h := h.LegacyHandler(strings.Split(m.Content[1:], ".")[0]); h != nil {
			h(s, m)
			return
		}
	})

	return errs.ErrorOrNil()
}
