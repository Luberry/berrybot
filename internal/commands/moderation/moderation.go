package moderation

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gitlab.com/luberry/berrybot/internal/commands"
	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/config/app"
	"gitlab.com/luberry/berrybot/internal/util"
)

var handler config.Configureable = &Handler{}
var _ commands.Handler = handler.(*Handler)

func init() {
	config.Register(handler)
}

type Handler struct {
	Enabled bool `yaml:"enabled"`
	log     *logrus.Entry

	appInfo *app.Info
}

const (
	say          = "say"
	members      = "members"
	chn          = "channel"
	msgCmd       = "message"
	defaultLimit = 10
)

func (h *Handler) Defaults() {
	h.Enabled = true
}
func (h *Handler) SetAppInfo(inf *app.Info) {
	h.appInfo = inf
}
func (h *Handler) Commands(_ *discordgo.Session) []*discordgo.ApplicationCommand {
	return nil
}
func (h *Handler) Name() string {
	return "moderation"
}
func (h *Handler) Init(ctx context.Context) error {
	h.log = logrus.WithField("name", h.Name())
	return nil
}
func (h *Handler) ModCommands(s *discordgo.Session) []*discordgo.ApplicationCommand {
	if !h.Enabled {
		return nil
	}
	return modCommands
}
func (h *Handler) Handler(_ string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	return nil
}
func (h *Handler) LegacyHandler(_ string) func(s *discordgo.Session, i *discordgo.MessageCreate) {
	return nil
}
func (h *Handler) ModHandler(n string) func(s *discordgo.Session, i *discordgo.InteractionCreate) {
	if !h.Enabled {
		return nil
	}
	switch n {
	case say:
		return func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			opts := i.ApplicationCommandData().Options
			if len(opts) != 2 {
				util.InteractionRespond(s, i.Interaction, "invalid options provided for say")
				return
			}
			options := make(map[string]*discordgo.ApplicationCommandInteractionDataOption)
			for _, opt := range opts {
				options[opt.Name] = opt
			}
			chn := util.GetInteractionOptChannel(s, options, chn)
			msg := util.GetInteractionOptString(options, msgCmd)
			if _, err := s.ChannelMessageSend(chn.ID, msg); err != nil {
				util.InteractionRespond(s, i.Interaction, err.Error())
				return
			}
			util.InteractionRespond(s, i.Interaction, fmt.Sprintf("sent message to %s", chn.Mention()))

		}
	case members:
		return func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			util.InteractionRespond(s, i.Interaction, "fetching member count")
			data, err := s.RequestWithBucketID("GET", discordgo.EndpointGuild(h.appInfo.GuildID)+"?with_counts=true", nil, discordgo.EndpointGuild(h.appInfo.GuildID))
			if err != nil {
				if _, err := s.InteractionResponseEdit(i.Interaction, &discordgo.WebhookEdit{Content: util.StringPTR(err.Error())}); err != nil {
					h.log.WithError(err).WithField("interaction", i).Error("failed to send response")
				}
				return
			}
			g := &discordgo.Guild{}

			if err := json.Unmarshal(data, g); err != nil {
				if _, err := s.InteractionResponseEdit(i.Interaction, &discordgo.WebhookEdit{Content: util.StringPTR(err.Error())}); err != nil {
					h.log.WithError(err).WithField("interaction", i).Error("failed to send response")
				}
				return
			}

			if _, err := s.InteractionResponseEdit(i.Interaction, &discordgo.WebhookEdit{
				Content: util.StringPTR(fmt.Sprintf("%s currently has %d total members. %d members are currently online.", h.appInfo.ServerName, g.ApproximateMemberCount, g.ApproximatePresenceCount)),
			}); err != nil {
				h.log.WithError(err).WithField("interaction", i).Error("failed to send response")
			}
		}
	}
	return nil
}

var (
	modCommands = []*discordgo.ApplicationCommand{
		{
			Name:        say,
			Description: "send a message as the bot to a channel",

			Options: []*discordgo.ApplicationCommandOption{
				{
					Name:        chn,
					Description: "channel to send message to",
					Type:        discordgo.ApplicationCommandOptionChannel,
					Required:    true,
				},
				{
					Name:        msgCmd,
					Description: "message to send",
					Type:        discordgo.ApplicationCommandOptionString,
					Required:    true,
				},
			},
		},
		{
			Name:        members,
			Description: "get current member count of the server",
		},
	}
)
