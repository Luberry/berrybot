ARG GO_VERS=1.22
ARG ALPINE_VERS=3.14
FROM golang:${GO_VERS}-alpine${ALPINE_VERS} as builder
ENV CGO_ENABLED=0
WORKDIR /
RUN apk add -U --no-cache ca-certificates
ADD go.mod go.sum /
RUN go mod download
ADD main.go /
ADD internal /internal
RUN go build -o /berrybot
RUN mkdir /cfg
FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /berrybot /berrybot
COPY --from=builder /cfg /cfg
ENTRYPOINT ["/berrybot"]


