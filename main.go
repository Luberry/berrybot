package main

import (
	"context"
	"os"
	"os/signal"

	// Plugin Imports

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"

	"gitlab.com/luberry/berrybot/internal/commands"
	_ "gitlab.com/luberry/berrybot/internal/commands/contribute"
	_ "gitlab.com/luberry/berrybot/internal/commands/moderation"
	_ "gitlab.com/luberry/berrybot/internal/commands/user"
	"gitlab.com/luberry/berrybot/internal/config"
	"gitlab.com/luberry/berrybot/internal/join"
	_ "gitlab.com/luberry/berrybot/internal/join/roles"
	_ "gitlab.com/luberry/berrybot/internal/join/spam"
	_ "gitlab.com/luberry/berrybot/internal/join/welcome"
	"gitlab.com/luberry/berrybot/internal/messages"
	_ "gitlab.com/luberry/berrybot/internal/messages/blacklist"
	_ "gitlab.com/luberry/berrybot/internal/messages/gallery"
	_ "gitlab.com/luberry/berrybot/internal/messages/keywordredirect"
	_ "gitlab.com/luberry/berrybot/internal/messages/scamfilter"
	"gitlab.com/luberry/berrybot/internal/reactions"
	_ "gitlab.com/luberry/berrybot/internal/reactions/roles"
	_ "gitlab.com/luberry/berrybot/internal/reactions/star_tracker"
)

var cfg = &config.Bot{}

var s *discordgo.Session
var ctx context.Context
var cancel context.CancelFunc

func init() {
	config.Register(cfg)
	ctx, cancel = context.WithCancel(context.Background())
	config.Load(ctx)
	var err error
	if s, err = discordgo.New("Bot " + cfg.Token); err != nil {
		logrus.WithError(err).Error("failed to configure discord bot")
	}
	if config.CleanCommands {
		logrus.Info("cleaning commands")
		if err := commands.CleanCommands(s, cfg.AppInfo.AppID, cfg.AppInfo.GuildID); err != nil {
			logrus.WithError(err).Fatal("failed to cleanup commands")
		}
	}
	if err = config.ForEach(func(c config.Configureable) error {
		logrus.WithField("name", c.Name()).Debug("registering commands")
		c.SetAppInfo(&cfg.AppInfo)
		if ch, ok := c.(commands.Handler); ok {
			err = commands.Register(s, &cfg.AppInfo, &cfg.Moderation, ch)
			if err != nil {
				return err
			}
		}
		if rh, ok := c.(reactions.Handler); ok {
			err = reactions.Register(s, rh)
			if err != nil {
				return err
			}
		}
		if jh, ok := c.(join.Handler); ok {
			err = join.Register(s, jh, &cfg.IntroConfig)
			if err != nil {
				return err
			}
		}
		if mh, ok := c.(messages.Handler); ok {
			err = messages.Register(s, mh)
			if err != nil {
				return err
			}
		}
		return nil
	}); err != nil {
		logrus.Fatal(err)
	}
}

func main() {
	defer cancel()
	if err := s.Open(); err != nil {
		logrus.WithError(err).Fatal("could not open session")
	}
	defer s.Close()
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	<-stop

}
